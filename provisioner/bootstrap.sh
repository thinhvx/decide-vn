#
#   settup python packages
#
sudo apt-get install -y build-essential
sudo apt-get install -y python-dev
sudo apt-get install -y libxslt-dev
sudo apt-get install -y libxml2-dev libxslt1-dev python-lxml
sudo apt-get install -y python-pip
sudo apt-get install -y python-virtualenv
sudo apt-get install -y python-imaging

sudo pip install django==1.4
sudo pip install scrapy

sudo pip install django-celery
sudo pip install south
sudo pip install scrapyd
sudo pip install django-dynamic-scraper
sudo pip install unidecode
sudo pip install BeautifulSoup
sudo pip install pytest
sudo pip install service_identity
sudo pip install w3lib
sudo pip install cssselect

sudo pip install -U textblob
sudo pip install fuzzywuzzy

#
#   settup java
#
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update
sudo apt-get install -y oracle-java7-installer

sudo pip install ScrapyElasticSearch
sudo pip install elasticsearch

#
# settup phantomJS casperJS
#
sudo add-apt-repository -y ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install -y nodejs
sudo apt-get install -y npm

sudo apt-get update
sudo apt-get install -y build-essential chrpath git-core libssl-dev libfontconfig1-dev libxft-dev
sudo npm install -g phantomjs
sudo npm install -g casperjs
sudo npm install -g sleep


#
# disable ipv6 faster phantomjs
#
sudo echo 1 > /proc/sys/net/ipv6/conf/all/disable_ipv6
sudo echo 1 > /proc/sys/net/ipv6/conf/default/disable_ipv6

#
# settup selenium
#
sudo pip install selenium
sudo apt-get install -y xvfb
sudo su
apt-get remove iceweasel
echo -e "\ndeb http://downloads.sourceforge.net/project/ubuntuzilla/mozilla/apt all main" | tee -a /etc/apt/sources.list > /dev/null
apt-key adv --recv-keys --keyserver keyserver.ubuntu.com C1289A29
apt-get update
apt-get install -y firefox-mozilla-build libdbus-glib-1-2 libgtk2.0-0 libasound2

sudo pip install pyvirtualdisplay
