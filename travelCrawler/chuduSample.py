__author__ = 'thinhvoxuan'
from selenium import selenium
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait


DATE_TIME_FORMAT = '%d/%m/%Y'

def process_text(text):
    import re
    text = re.sub(' +', ' ', text)
    text = re.sub('\n', ' ', text)
    text = re.sub('\r', ' ', text)
    return text


def get_text(dr, css_selector):
    try:
        sel = dr.find_element_by_css_selector(css_selector)
        return_text = process_text(sel.text)
    except Exception:
        return_text = ''
    finally:
        return return_text


def get_detail_in_date(ct):
    try:
        roomTag = 'tr.room'
        nameTag = 'span.ten-phong'
        soNguoiTag = 'td.nguoi'
        giaBinhThuongTag = 'td.normal-rate'
        memberRateTag = 'td.member-rate div'

        return_dict = {}

        for eachRow in ct.find_elements_by_css_selector(roomTag):
            nameText = get_text(eachRow, nameTag)
            soNguoiText= get_text(eachRow, soNguoiTag)
            giaBinhThuongText = get_text(eachRow, giaBinhThuongTag)
            memberRateText = get_text(eachRow, memberRateTag)
            return_dict[nameText] = [nameText, soNguoiText, giaBinhThuongText, memberRateText]

        return return_dict
    except Exception:
        return {}



def crawl_chudu(link, from_date_str, to_date_str):
    from datetime import datetime, timedelta

    all_infor = {}

    current_date = datetime.strptime(from_date_str, DATE_TIME_FORMAT)
    end_date = datetime.strptime(to_date_str, DATE_TIME_FORMAT)

    while current_date < end_date:

        next_date = current_date + timedelta(days=1)

        next_date_str = next_date.strftime(DATE_TIME_FORMAT)
        current_date_str = current_date.strftime(DATE_TIME_FORMAT)

        all_infor[current_date_str] = get_information(link, current_date_str, next_date_str)

        #
        # increase date
        #
        current_date = next_date

    return all_infor


def get_information(link, from_date_str, to_date_str):
    from pyvirtualdisplay import Display
    from datetime import datetime, timedelta
    display = Display()
    display.start()

    driver = webdriver.Firefox()

    driver.get(link)
    button = driver.find_element_by_css_selector('button.search-button')
    driver.execute_script('$("input.check-in").datepicker("setDate", "' + from_date_str + '" );')
    driver.execute_script('$("input.check-out").datepicker("setDate","' + to_date_str + '" );')

    button.click()
    return_dict = {}
    try:

        wait = WebDriverWait(driver, 20, 1)
        roomType = 'table.search-result-roomtype'
        wait.until(lambda d: d.find_element_by_css_selector(roomType))
        content = driver.find_element_by_css_selector(roomType)
        return_dict = get_detail_in_date(content)

    except Exception:
        pass

    finally:

        driver.close()
        driver.quit()
        display.stop()

        return return_dict

if __name__ == '__main__':
    # sampleLink = [
    #     'http://khachsan.chudu24.com/ks/256/vinpearl-resort-nha-trang.html',
    #     'http://khachsan.chudu24.com/ks/1766/khach-san-havana-nha-trang.html',
    #     'http://khachsan.chudu24.com/ks/378/six-senses-ninh-van-bay.html',
    #     'http://khachsan.chudu24.com/ks/1009/amiana-resort-nha-trang.html',
    #     'http://khachsan.chudu24.com/ks/1067/mia-resort-nha-trang.html'
    # ]
    # for eachLink in sampleLink:
    #     print get_information(eachLink)


    link = 'http://khachsan.chudu24.com/ks/256/vinpearl-resort-nha-trang.html'
    result = crawl_chudu(link, '20/12/2014', '10/01/2015')
    f = open('output-chudu.txt', 'w')
    import json
    json.dump(result, f, sort_keys=True, indent=4, separators=(',', ': '))