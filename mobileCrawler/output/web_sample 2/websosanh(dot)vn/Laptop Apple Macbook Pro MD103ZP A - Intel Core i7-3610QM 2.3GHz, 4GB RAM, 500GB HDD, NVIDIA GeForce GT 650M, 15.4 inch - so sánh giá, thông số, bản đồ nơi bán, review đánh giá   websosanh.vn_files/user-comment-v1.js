﻿var UserCommentLogType = {
    "F_LOGIN": 1,
    "G_LOGIN": 2,
    "LOGOUT": 3,
    "POST_COMMENT": 4,
    "REPLY_COMMENT": 5,
    "VOTE_YES_NO": 6,
    "CHANGE_DISPLAYNAME": 7,
    "EDIT_ACCOUNT": 8,
    "EDIT_EMAIL_NOTIFICATION": 9,
    "CHANGE_PASS": 10,
    "EDIT_PROFILE": 11,
    "EDIT_COMMENT": 12,
    "VIEW_PRODUCT": 13,
    "VIEW_NEWS": 14,
    "RATE_NEWS": 15
};

var error1 = "<span class='rf-error-text'>Hãy chọn một điểm đánh giá bằng cách nhấn vào những ngôi sao.</span><br />";
var error2 = "<span class='rf-error-text'>Bạn chưa nhập bình luận.</span><br />";
var error3 = "<span class='rf-error-text'>Bình luận của bạn phải dài ít nhất 15 từ.</span><br />";
var minWords = 15;
//show more rating
$(document).on('click', ".review-more-toggle", function () {
    var more = $(this).siblings(".rr-more-data");
    //    more.slideToggle();
    if (more.is(':hidden')) {
        more.slideDown();
    } else more.slideUp();
});
//pagging
$(document).on("click", "#all-rate .paging > a", function () {
    var idUser = $("#userID").val();
    var sortExpr = "";
    if ($("#ddlSortComment").length > 0) {
        sortExpr = $("#ddlSortComment").val();
    }
    addCurrentPage($(this));
    var p = $(this).parents(".all-rate");
    //        pagingEffect($(".rr-pagination"));
    paging($("#objectID").val(), $("#commentType").val(), $("#hdfVote").val(), $(".paging a.current").attr('data-page'), sortExpr);
    return false;
});
function paging(o, t, v, p, s) {
    $.ajax({
        url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/CommentPaging",
        dataType: "jsonp",
        data: { objectID: o,
            //            idUser: idUser,
            typeID: t,
            vote: v,
            pageIndex: p,
            sortExpr: s
        },
        success: function (result) {
            $("#all-rate .all-rate").html(result);
            $('html, body').animate({
                scrollTop: $(".all-rate").offset().top
            }, 500);
        },
        error: ServiceFailed
    });
}
$(document).on("click", "#rates .rate-static .rate-filter", function () {
    $("#hdfVote").val($(this).attr("value"));
    var idUser = $("#userID").val();
    var sortExpr = "";
    if ($("#ddlSortComment").length > 0) {
        sortExpr = $("#ddlSortComment").val();
    }
    addCurrentPage($(this));
    var p = $(".all-rate");
    //        pagingEffect($(".rr-pagination"));
    $.ajax({
        url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/CommentPaging",
        dataType: "jsonp",
        data: { objectID: $("#objectID").val(),
            //            idUser: idUser,
            typeID: $("#commentType").val(),
            vote: $("#hdfVote").val(),
            pageIndex: 1,
            sortExpr: sortExpr
        },
        success: function (result) {
            p.html(result);
            $('html, body').animate({
                scrollTop: p.offset().top
            }, 500);
        },
        error: ServiceFailed
    });
});
//end paging
//ready
$(document).ready(function () {
    $("#ddlSortComment").val("PublicDate");
    $("#hdfVote").val("0");
    $("#ddlSortComment").change(function () {
        var page1 = $(".paging > a[data-page='1']");
        if (page1.length > 0) {
            page1.click();
        }
    });
    //rating
    $(".ratable").find(".vote-range").each(function () {
        setColorWidth($(this), $(this).closest(".ratable").attr("data-rate"));
    });
    //    $(document).on("mouseover", ".ratable .vote-star", function () {
    //        setColorWidth($(this).closest(".ratable").find(".vote-range"), $(this).attr("data-rate"));
    //    });
    //    $(".ratable .vote-star").each(function() {
    //    });
    $(".ratable .vote-star").mouseover(function () {
        setColorWidth($(this).closest(".ratable").find(".vote-range"), $(this).attr("data-rate"));
    });
    $(".ratable .vote-star").click(function () {
        $(this).parents(".ratable").attr("data-rate", $(this).attr("data-rate"));
        setColorWidth($(this).closest(".ratable").find(".vote-range"), $(this).closest(".ratable").attr("data-rate"));
    });
    $(".ratable").mouseleave(function () {
        setColorWidth($(this).find(".vote-range"), $(this).attr("data-rate"));
    });

    //end rating

    //textarea
    $(".rf-word-count").hide();
    $(document).on('focus', "#all-rate textarea.rf-review-body", function () {
        $(this).siblings(".rf-word-count").show();
    });
    $(document).on('focusout', "#all-rate textarea.rf-review-body", function () {
        if ($(this).val() == "") {
            $(this).siblings(".rf-word-count").hide();
        }
    });
    //textarea
    var wordCounts = {};
    $(document).on('keyup', "#all-rate textarea.rf-review-body", function () {
        var matches = this.value.match(/\b/g);
        wordCounts[this.id] = matches ? matches.length / 2 : 0;
        var finalCount = 0;
        $.each(wordCounts, function (k, v) {
            finalCount += v;
        });
        finalCount = 15 - finalCount;
        $(this).siblings(".rf-word-count").html("Xin viết tối thiểu " + finalCount + " từ nữa");
        minWords = finalCount;
        if (finalCount < 1) {
            $(this).siblings(".rf-word-count").html("");
        }
    });
    //end textarea
    //post comment
    $("#btn-compare").click(function () {
        if (validateReview()) {
            if (!$("#userID").val()) {
                //                $.colorbox({
                //                    transition: "none",
                //                    inline: true,
                //                    fixed: true,
                //                    href: "#return-login",
                //                    innerHeight: '270px',
                //                    innerWidth: '480px'
                //                                });

                $('html, body').animate({
                    scrollTop: $("#return-login").offset().top
                }, 500);
                $(".login").addClass("login-show-note");
                var valueComment = [$(".ratable").attr('data-rate'),
                $(".rf-review-body").val(),
                $("#commentType").val(),
                $("#objectID").val(),
                "0"];
                writeCookie('valueComment', JSON.stringify(valueComment), 3);
            } else {
                processing();
                var rate1 = ($("#rate1").length > 0) ? $("#rate1").attr('data-rate') : 0;
                var rate2 = ($("#rate2").length > 0) ? $("#rate2").attr('data-rate') : 0;
                var rate3 = ($("#rate3").length > 0) ? $("#rate3").attr('data-rate') : 0;
                var rate4 = ($("#rate4").length > 0) ? $("#rate4").attr('data-rate') : 0;
                $.ajax({
                    url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/PostComment",
                    dataType: "jsonp",
                    data: {
                        rate0: $("#rate0").attr('data-rate'),
                        txtComment: $(".rf-review-body").val(),
                        txtCommentBad: $(".text2").val(),
                        rate1: rate1,
                        rate2: rate2,
                        rate3: rate3,
                        rate4: rate4,
                        commentType: $("#commentType").val(),
                        productid: $("#objectID").val(),
                        idComment: 0,
                        isShare: $('#isShare').is(":checked")
                    },
                    success: function (result) {
                        if (result != "") {
                            $("#hdfVote").val("0");
                            $("#ddlSortComment").val("PublicDate");
                            ResetControl();
                            var replyContent = result.NoiDung;

                            $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.POST_COMMENT }, function (result) {
                            });
                            $.post("/Ajax/User/AutoSendMail.ashx", { idComment: result.ID, replyContent: replyContent, commentType: $("#commentType").val(), objectid: $("#objectID").val(), parentComment: 0 }, function (result) {
                            });
                            $(".rating").hide();
                            paging($("#objectID").val(), $("#commentType").val(), $("#hdfVote").val(), 1, "PublicDate");
                        }
                        processinghide();
                    },
                    error: ServiceFailed
                });
            }
        }
        return false;
    });

    //reply comment
    $(document).on('click', ".reply-comment", function () {
        if (!$("#userID").val()) {
            //            $.colorbox({
            //                transition: "none",
            //                inline: true,
            //                fixed: true,
            //                href: "#aregister"
            //            });
            $('html, body').animate({
                scrollTop: $("#return-login").offset().top
            }, 500);
            $(".login").addClass("login-show-note");
            return false;
        }
        else {
            ShowTextareaReply($(this));
        }
    });
    $(document).on('keyup', ".rr-textarea", function () {
        if ($(this).val() != "") {
            $(this).siblings(".rr-submit").show();
        }
        else {
            $(this).siblings(".rr-submit").hide();
        }
    });
    $(document).on('click', ".rr-submit", function () {
        if (!$("#userID").val()) {
            //                $.colorbox({
            //                    transition: "none",
            //                    inline: true,
            //                    fixed: true,
            //                    href: "#aregister"
            //                });
            $('html, body').animate({
                scrollTop: $("#return-login").offset().top
            }, 500);
            $(".login").addClass("login-show-note");
            return false;
        }
        else {
            processing();
            var p = $(this).parents(".all-rate-item");
            if (p.find("textarea").val() != "") {
                $.ajax({
                    url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/PostComment",
                    dataType: "jsonp",
                    data: {
                        rate0: 0,
                        txtComment: p.find("textarea").val(),
                        txtCommentBad: "",
                        rate1: 0,
                        rate2: 0,
                        rate3: 0,
                        rate4: 0,
                        commentType: $("#commentType").val(),
                        productid: $("#objectID").val(),
                        idComment: p.attr("value")
                    },
                    success: function (result) {
                        $(".rr-textarea").parents(".rr-reply-form").slideUp();
                        var a = p.find(".rr-comments .rr-comment:last");
                        if (a.length) {
                            a.after(result);
                        }
                        else {
                            p.find(".rr-comments").prepend(result);
                        }
                        var url = location.href.split('#')[0] + "#" + p.attr("value");
                        var replyContent = "";
                        var replyContentPar = p.find(".rr-comments .rr-comment:last .rr-rev");
                        if (replyContentPar.find(".teaser-content").length > 0) {
                            replyContent = replyContentPar.find(".teaser-content").html() + replyContentPar.find(".complete-content").html();
                        }
                        else {
                            replyContent = replyContentPar.html();
                        }
                        $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.REPLY_COMMENT, viewUrl: url }, function (result) { });
                        $.post("/Ajax/User/AutoSendMail.ashx", { parentComment: p.attr("value"), replyContent: replyContent }, function (result) { });
                        processinghide();
                    },
                    error: ServiceFailed
                });
            }
            else {
                p.find("textarea").focus();
                return false;
            }
        }
    });
    //end reply comment

});
//end ready
//
//show popup viewmore
$(document).on('click', "#rates .rev-link", function () {
    var id = $(this).attr("value");
    showComment(id);
});
function showComment(id) {
    var containDiv = $("#all-rate .all-rate-item[value='" + id + "']");
    if (containDiv.length > 0) {
        $('html, body').animate({
            scrollTop: containDiv.offset().top
        }, 500);
    } else {
        $.post("/Ajax/V1/UserComment/UserCommentServiceV1.asmx/PopupComment", { commentid: id },
            function (result) {
                var html = "<div class='popup-review'><div class='all-rate'>" + result + "</div></div>";
                $.colorbox({
                    scrolling: true,
                    preloading: false,
                    fixed: true,
                    innerHeight: '80%',
                    innerWidth: '60%',
                    html: html
                });
                $(".rr-textarea").parents(".rr-reply-form").hide();
            }, 'jsonp');
    }
}
//end popup viewmore
//show more text Noidung
$(document).on('click', ".rr-read-more .link", function () {
    var par = $(this).parents(".rr-read-more");
    if (par.hasClass("view-more")) {
        par.siblings(".complete-content").show();
        par.removeClass("view-more");
    }
    else {
        par.siblings(".complete-content").hide();
        par.addClass("view-more");
    }
});
//end show more text Noidung
//show more reply
$(document).on("click", ".more-re-review-show", function () {
    var replyDiv = $(this).parents(".rr-comments");
    $.ajax({
        url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/ShowAllReplyComment",
        dataType: "jsonp",
        data: { objectid: $("#objectID").val(),
            commentType: $("#commentType").val(),
            parentComment: replyDiv.parents(".all-rate-item").attr("value"),
            isShowMore: true
        },
        success: function (result) {
            replyDiv.find(".rr-comment").remove();
            replyDiv.find(".more-re-review-show").before(result);
            replyDiv.slideDown("slow");
            replyDiv.find(".more-re-review-show").hide();
        },
        error: ServiceFailed
    });
});
//end show more reply
//yes-no vote
$(document).on('click', ".rr-vote", function () {
    if (!$("#userID").val()) {
        //        $.colorbox({
        //            transition: "none",
        //            inline: true,
        //            fixed: true,
        //            href: "#aregister"
        //        });
        $('html, body').animate({
            scrollTop: $("#return-login").offset().top
        }, 500);
        $(".login").addClass("login-show-note");
        return false;
    }
    else {
        var selector = $(this);
        var idComment = selector.attr("value");
        if (selector.hasClass("up")) {
            $.ajax({
                url: "/Ajax/User/VoteYesNo.ashx",
                dataType: "jsonp",
                data: { vote: 'true',
                    userID: $("#userID").val(),
                    idComment: idComment
                },
                success: function (result) {
                    selector.closest(".vote-icon").find(".rr-count").remove();
                    selector.append(result.TotalVoteGood);
                    selector.siblings(".down").append(result.TotalVoteBad);
                    var url = location.href.split('#')[0] + "#" + idComment;
                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.VOTE_YES_NO, viewUrl: url }, function (result) { });
                    $.post("/Ajax/User/AutoSendMail.ashx", { idComment: idComment, vote: 'true', isVote: 'true' }, function (result) { });
                },
                error: ServiceFailed
            });
        }
        if (selector.hasClass("down")) {
            $.ajax({
                url: "/Ajax/User/VoteYesNo.ashx",
                dataType: "jsonp",
                data: { vote: 'false',
                    userID: $("#userID").val(),
                    idComment: idComment
                },
                success: function (result) {
                    selector.closest(".vote-icon").find(".rr-count").remove();
                    selector.append(result.TotalVoteBad);
                    selector.siblings(".up").append(result.TotalVoteGood);
                    var url = location.href.split('#')[0] + "#" + idComment;
                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.VOTE_YES_NO, viewUrl: url }, function (result) { });
                    $.post("/Ajax/User/AutoSendMail.ashx", { idComment: idComment, vote: 'false', isVote: 'true' }, function (result) { });
                },
                error: ServiceFailed
            });
        }
    }
});
//end yes-no vote
function setColorWidth(s, v) {
    var w = parseInt(v) * 20;
    s.css("width", w + "%");
}
function addCurrentPage(s) {
    $(".paging a").removeClass("current");
    s.addClass("current");
}
//validate rate
function validateRate(s) {
    if (s.length > 0) {
        if (s.attr("data-rate") == 0) {
            s.parents(".inport-rate").addClass("error");
            return error1;
        } else {
            s.parents(".inport-rate").removeClass("error");
            return "";
        }
    }
    else {
        return "";
    }
}
//validate text news
function validateText(s, w) {
    if (w >= 15) {
        s.parents(".inport-text").addClass("error");
        return error2;
    } else if (w > 0) {
        s.parents(".inport-text").addClass("error");
        return error3;
    } else {
        s.parents(".inport-text").removeClass("error");
        return "";
    }
}
//validate all
function validateReview() {
    $("textarea").keyup();
    var htmlError = validateRate($(".vote.ratable")) + validateText($("textarea.rf-review-body"), minWords);
    //    }
    if (htmlError != "") {
        $(".error-div").css('display', 'block');
        $(".error-div").html(htmlError);
        return false;
    } else {
        $(".error-div").css('display', 'none');
        return true;
    }
}

//call when load
function afterLoad() {
    var a = readCookie('valueComment');
    writeCookie('valueComment', "", -1);
    if (a != "") {
        var arr = JSON.parse(a);
        if (arr.length > 9) {
            addComment(arr[0], arr[1], arr[2], arr[3], arr[4]);
        }
    }
}

$(window).load(function () {
    afterLoad();
});
//cookie
function writeCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}
//end cookie
//add comment from cookie
function addComment(rate0, txtComment, commentType, productid, idComment) {
    var isCommented = $("#CheckCommented").val();
    if (isCommented != "False") {
        if (commentType == 1 || commentType == 3) {
            alert("Bạn không thể thêm đánh giá.");
        }
        else {
            alert("Bạn không thể thêm bình luận.");
        }
    }
    else {
        var isValid = false;
        if (commentType == "1" || commentType == "3") {
            if (rate0 != "" && (txtComment != "")) {
                isValid = true;
            }
        }
        if (isValid) {
            $.ajax({
                url: "/Ajax/V1/UserComment/UserCommentServiceV1.asmx/PostComment",
                dataType: "jsonp",
                data: {
                    rate0: rate0,
                    txtComment: txtComment,
                    txtCommentBad: "",
                    rate1: 0,
                    rate2: 0,
                    rate3: 0,
                    rate4: 0,
                    commentType: commentType,
                    productid: productid,
                    idComment: idComment
                },
                success: function (result) {
                    if (result != "") {
                        ResetControl();
                        $('html, body').animate({
                            scrollTop: $(".all-rate").offset().top
                        }, 500);
                        var replyContent = result.NoiDung;
                        $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.POST_COMMENT }, function (r) { });
                        $.post("/Ajax/User/AutoSendMail.ashx", { idComment: result.ID, replyContent: replyContent, commentType: commentType, objectid: productid, parentComment: 0 }, function (r) { });

                        $(".rating").hide();
                        paging($("#objectID").val(), $("#commentType").val(), $("#hdfVote").val(), 1, "PublicDate");
                    }
                },
                error: ServiceFailed
            });
        }
    }
}
//reset comment input controls
function ResetControl() {
    $("p.ratable").attr("data-rate", "0");
    $("p.ratable .vote-range").css("width", "0%");
    $("textarea.rf-review-body").val("");
    minWords = 15;
}

//reply comment
function ShowTextareaReply(s) {
    var text = "";
    if (s.hasClass("reply-to")) {
        var name = s.parents(".comment-name").find(".rr-comment-username").html();
        text = "@" + name + ":";
    }
    var more = s.parents(".all-rate-item").find(".rr-reply-form");
    more.find('textarea').val(text);
    more.slideToggle();
    more.parents(".rr-reply-form").slideUp();
    more.find(".rr-submit").hide();
}
//end reply
//loading
function processing() {
    var modal = $('<div />');
    modal.addClass("modal");
    $('body').append(modal);
    var loading = $(".loading");
    loading.show();
    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
    loading.css({ top: top, left: left });
}
function processinghide() {
    $(".loading").hide();
    $(".modal").hide().removeClass("modal");
}

//cut string by word
function catSapo(input, sotu) {
    if (input == "") {
        return "";
    }
    var strArray = input.Split(' ');
    if (strArray.Length <= sotu) {
        return input;
    }
    return (strArray.slice(1, sotu).join(" "));
}

function viewShowMoreText(t) {
    var htmlContent = "";
    t = t.trim();
    if (t != "") {
        var teaser = catSapo(t, 60);
        var complete = t.replace(teaser, "");
        if (complete.trim() != "") {
            htmlContent = "<span class=\"teaser-content\">" + teaser + "</span>";
            htmlContent += "<span class=\"complete-content\">" + complete + "</span>";
            htmlContent += "<span class=\"rr-read-more view-more\"><span class='dots'>...</span><span class=\"link more-content\">Xem thêm</span><span class=\"link less-content\">Thu gọn</span><span class=\"arrow-view\"></span></span>";
        }
        else htmlContent = teaser;
    }
    return htmlContent;
}