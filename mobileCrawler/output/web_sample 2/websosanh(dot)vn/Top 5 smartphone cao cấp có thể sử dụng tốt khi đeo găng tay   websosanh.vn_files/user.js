﻿var UserCommentLogType = {
    "F_LOGIN": 1,
    "G_LOGIN": 2,
    "LOGOUT": 3,
    "POST_COMMENT": 4,
    "REPLY_COMMENT": 5,
    "VOTE_YES_NO": 6,
    "CHANGE_DISPLAYNAME": 7,
    "EDIT_ACCOUNT": 8,
    "EDIT_EMAIL_NOTIFICATION": 9,
    "CHANGE_PASS": 10,
    "EDIT_PROFILE": 11,
    "EDIT_COMMENT": 12,
    "VIEW_PRODUCT": 13,
    "VIEW_NEWS": 14,
    "RATE_NEWS": 15
};

var htmlError = "";
var minWords1 = 15;
var minWords2 = 15;

var error1 = "<span class='rf-error-text'>Hãy chọn một điểm đánh giá bằng cách nhấn vào những ngôi sao.</span><br />";
var error2 = "<span class='rf-error-text'>Bạn chưa nhập bình luận.</span><br />";
var error3 = "<span class='rf-error-text'>Bình luận của bạn phải dài ít nhất 15 từ.</span><br />";
var error4 = "<span class='rf-error-text'>Bạn hãy nhập ưu điểm hoặc nhược điểm của sản phẩm.</span><br />";
//comment state
var arrComState = [];
//var vote = 0;
var expertPageIndex = 1;
var timerCheck = 1;
var intervalTimerCheck;

function CheckTimer() {
    timerCheck = 1;
    clearInterval(intervalTimerCheck);
}
function addComment(rate0, txtComment, txtCommentBad, rate1, rate2, rate3, rate4, commentType, productid, idComment) {
    var isCommented = $("#CheckCommented").val();
    if (isCommented != "False") {
        if (commentType == 1 || commentType == 3) {
            alert("Bạn không thể thêm đánh giá.");
        }
        else {
            alert("Bạn không thể thêm bình luận.");
        }
    }
    else {
        var isValid = false;
        if (commentType == "1" || commentType == "3") {
            if (rate0 != "" && (txtComment != "" || txtCommentBad != "")) {
                isValid = true;
            }
        }
        else {
            rate0 = rate1 = rate2 = rate3 = rate4 = 0;
            if (txtComment != "") {
                isValid = true;
            }
        }
        //    if (timerCheck) {
        //        timerCheck = 0;
        //        intervalTimerCheck = setInterval(CheckTimer, 30000);
        if (isValid) {
            $.ajax({
                url: "/Ajax/User/PostComment.ashx",
                dataType: "jsonp",
                data: {
                    rate0: rate0,
                    txtComment: txtComment,
                    txtCommentBad: txtCommentBad,
                    rate1: rate1,
                    rate2: rate2,
                    rate3: rate3,
                    rate4: rate4,
                    commentType: commentType,
                    productid: productid,
                    idComment: idComment
                },
                success: function (result) {
                    //                    timerCheck = 0;
                    //                    intervalTimerCheck = setInterval(CheckTimer, 30000);
                    if (result != "") {
                        $(".rr-pagination > span[data-page='1']").click();
                        $(".reviews").prepend(result);
                        //                $(".reviews .review:last").remove();
                        ResetControl($(".rf-review-wrap"));
                        CheckUserType();
                        $(".rr-textarea").parents(".rr-reply-form").hide();
                        countComment();
                        newsComment();
                        resetHeight();
                        $('html, body').animate({
                            scrollTop: $(".ListComments").offset().top - 100
                        }, 500);
                        var replyContent = "";
                        var replyContentPar = $(".reviews").find(".review:first .rr-rev");
                        if (replyContentPar.find(".teaser-content").length > 0) {
                            replyContent = replyContentPar.find(".teaser-content").html() + replyContentPar.find(".complete-content").html();
                        }
                        else {
                            replyContent = replyContentPar.html();
                        }
                        $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.POST_COMMENT }, function (result) { });
                        $.post("/Ajax/User/AutoSendMail.ashx", { idComment: $(".reviews").find(".review:first").attr("value"), replyContent: replyContent, commentType: commentType, objectid: productid, parentComment: 0 }, function (result) { });
                    }
                },
                error: ServiceFailed
            });
        }
        //    }
        //    else {
        //        alert("Xin chờ 30 giây để tiếp tục đăng bình luận.");
        //    }
    }
}

function afterLoad() {
    var a = readCookie('valueComment');
    writeCookie('valueComment', "", -1);
    if (a != "") {
        var arr = JSON.parse(a);
        if (arr.length > 9) {
            addComment(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6], arr[7], arr[8], arr[9]);
        }
    }
}

$(window).load(function () {
    afterLoad();
    scrollToReview();
});

//cookie
function writeCookie(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookie(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}
//end cookie


$(document).ready(function () {
    expertPageIndex = 1;
    //    vote = 0;
    //alert(document.cookie)
    $(".rf-help-review").remove();
    resetHeight();

    $(".rating-title").each(function () {
        if (!$(this).next("div").length) {
            var p = $(this).parents(".rating-overview").remove();
        }
    });
    $(".ro-sub-title").each(function () {
        var p = $(this).parents(".half");
        if (p.hasClass("ro-expert")) {
            if (!$(this).next("div").length) {

                p.html("<h3 class=\"ro-sub-title\"> Chuyên gia đánh giá</h3><div class=\"no-data\">Chuyên gia</div>");
            }
        }
        if (p.hasClass("ro-users")) {
            if (!p.find(".rating-overview").length) {
                p.html("<h3 class=\"ro-sub-title\"> Người dùng đánh giá</h3><div class=\"no-data\"><a href='#ratingcheck'>Bạn hãy là người đầu tiên đánh giá sản phẩm này.</a> </div>");
            }
        }
    });
    //check no data reviews
    countComment();
    //if is news comments
    newsComment();
    // show help
    $(".rf-help-review").hide();
    $(".rf-word-count").hide();
    $(".rf-attr-ratings").hide();
    $(document).on('focus', "textarea.rf-review-body", function () {
        $(this).siblings(".rf-word-count").show();
        //        $(".rf-help-review").toggle("slide");
        $(".rf-help-review").show('slide', { direction: 'right' }, 500);
    });
    $(document).on('focusout', "textarea.rf-review-body", function () {
        if ($(this).val() == "") {
            $(this).siblings(".rf-word-count").hide();
        }
        //        $(".rf-help-review").toggle("slide");
        $(".rf-help-review").hide('slide', { direction: 'right' }, 500);
    });
    //textarea
    var wordCounts = {};
    $(document).on('keyup', "textarea.rf-review-body", function () {
        var matches = this.value.match(/\b/g);
        wordCounts[this.id] = matches ? matches.length / 2 : 0;
        var finalCount = 0;
        $.each(wordCounts, function (k, v) {
            finalCount += v;
        });
        finalCount = 15 - finalCount;
        $(this).siblings(".rf-word-count").html("Xin viết tối thiểu " + finalCount + " từ nữa");
        if ($(this).hasClass("text1")) {
            minWords1 = finalCount;
        }
        if ($(this).hasClass("text2")) {
            minWords2 = finalCount;
        }
        //validateText($(this), minWords);
        if (finalCount < 1) {
            $(this).siblings(".rf-word-count").html("");
            if (minWords1 >= 15) {
                $(".text1").parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
                $(".text1").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
            }
            if (minWords2 >= 15) {
                $(".text2").parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
                $(".text2").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
            }
            $(".rf-attr-ratings").slideDown();
            $(this).parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
            $(this).parents(".rf-wrap").find(".icon-yes").css('display', 'inline-block');
            resetHeight();
        }
    });

    $(".rr-textarea").parents(".rr-reply-form").hide();
    $(document).on('click', ".rr-reply-btn", function () {
        if (!$("#userID").val()) {
            $.colorbox({
                transition: "none",
                inline: true,
                fixed: true,
                href: "#aregister"
            });
            return false;
        }
        else {
            ShowTextareaReply($(this));
        }
    });
    $(document).on('click', ".reply-to", function () {
        if (!$("#userID").val()) {
            $.colorbox({
                transition: "none",
                inline: true,
                fixed: true,
                href: "#aregister"
            });
            return false;
        }
        else {
            ShowTextareaReply($(this));
        }
    });
    $(document).on('keyup', ".rr-textarea", function () {
        if ($(this).val() != "") {
            $(this).siblings(".rr-submit").show();
        }
        else {
            $(this).siblings(".rr-submit").hide();
        }
        resetHeight();
    });
    //check user
    CheckUserType();
    //textarea reply
    $(".rr-textarea").parents(".rr-reply-form").hide();

    checkCommentType();
    //display name
    $(".rf-dn-input").keyup(function () {
        $("#displayName").val($(this).val());
    });
    ///sort by default
    $("#ddlSortComment").val("PublicDate");
    $(".rate-news li").click(function () {
        if (!$("#userID").val()) {
            $.colorbox({
                transition: "none",
                inline: true,
                fixed: true,
                href: "#aregister"
            });
            return false;
        }
        else {
            $.ajax({
                url: "/Ajax/User/RateNews.ashx",
                dataType: "jsonp",
                data: {
                    rate0: $(this).attr('data-rate'),
                    commentType: $("#commentType").val(),
                    productid: $("#objectID").val()
                },
                success: function (result) {
                    $(".rate-news").html(result);
                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.RATE_NEWS, viewUrl: location.href }, function (result) { });
                },
                error: ServiceFailed
            });
        }
    });
    //    $(document).on('click', ".popup-review #rf-post-review", function () {
    //        if (validateUpdateReview()) {
    //            $.ajax({
    //                url: "/Ajax/User/PostComment.ashx",
    //                dataType: "jsonp",
    //                data: {
    //                    rate0: $(".popup-review #rate0").attr('data-value'),
    //                    txtComment: $(".popup-review .text1").val(),
    //                    txtCommentBad: $(".popup-review .text2").val(),
    //                    rate1: $(".popup-review #rate1").attr('data-value'),
    //                    rate2: $(".popup-review #rate2").attr('data-value'),
    //                    rate3: $(".popup-review #rate3").attr('data-value'),
    //                    rate4: $(".popup-review #rate4").attr('data-value'),
    //                    commentType: $("#commentType").val(),
    //                    productid: $("#objectID").val(),
    //                    //idComment: 0,
    //                    //isShare: $('#isShare').is(":checked"),
    //                    editID: $("#editCommentID").val(),
    //                    currentPage: $(".rr-pagination span.rr-current").attr("data-page")
    //                },
    //                success: function (result) {
    //                    $.colorbox.close();
    //                    $(".rr-pagination > span[data-page='" + result + "']").click();
    //                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.EDIT_COMMENT, viewUrl: location.href + "#" + $("#editCommentID").val() }, function (result) { });
    //                },
    //                error: ServiceFailed
    //            });
    //        }
    //    });

    function validateUpdateReview() {
        $("textarea").keyup();
        htmlError = validateRate($(".popup-review #rate0")) + validateText($(".popup-review .text1"), minWords1);
        if (htmlError != "") {
            $(".popup-review .rf-error-sec").css('display', 'block');
            $(".popup-review .rf-error-sec").html(htmlError);
            return false;
        } else {
            $(".popup-review .rf-error-sec").css('display', 'none');
            return true;
        }
    }
    //post comment
    $("#rf-post-review").click(function () {
        if (validateReview()) {
            //            if (timerCheck) {
            //                timerCheck = 0;
            //                intervalTimerCheck = setInterval(CheckTimer, 30000);
            if (!$("#userID").val()) {
                $.colorbox({
                    transition: "none",
                    inline: true,
                    fixed: true,
                    href: "#aregister"
                });
                var valueComment = [$("#rate0").attr('data-value'),
                                $(".text1").val(),
                                $(".text2").val(),
                                $("#rate1").attr('data-value'),
                                $("#rate2").attr('data-value'),
                                $("#rate3").attr('data-value'),
                                $("#rate4").attr('data-value'),
                                $("#commentType").val(),
                                $("#objectID").val(),
                                "0"]

                writeCookie('valueComment', JSON.stringify(valueComment), 3);
                return false;
            }
            else {
                processing();
                $.ajax({
                    url: "/Ajax/User/PostComment.ashx",
                    dataType: "jsonp",
                    data: {
                        rate0: $("#rate0").attr('data-value'),
                        txtComment: $(".text1").val(),
                        txtCommentBad: $(".text2").val(),
                        rate1: $("#rate1").attr('data-value'),
                        rate2: $("#rate2").attr('data-value'),
                        rate3: $("#rate3").attr('data-value'),
                        rate4: $("#rate4").attr('data-value'),
                        commentType: $("#commentType").val(),
                        productid: $("#objectID").val(),
                        idComment: 0,
                        isShare: $('#isShare').is(":checked")
                        //                        code: $("#hdCode").val()
                    },
                    success: function (result) {
                        //                            timerCheck = 0;
                        //                            intervalTimerCheck = setInterval(CheckTimer, 30000);
                        if (result != "") {
                            $("#hdfVote").val("0");
                            $("#ddlSortComment").val("PublicDate");
                            $(".rr-pagination > span[data-page='1']").click();
                            $(".reviews").prepend(result);
                            //                        $(".reviews .review:last").remove();
                            ResetControl($(".rf-review-wrap"));
                            CheckUserType();
                            $(".rr-textarea").parents(".rr-reply-form").hide();
                            countComment();
                            newsComment();
                            resetHeight();
                            $('html, body').animate({
                                scrollTop: $(".ListComments").offset().top - 100
                            }, 500);
                            var replyContent = "";
                            var replyContentPar = $(".reviews").find(".review:first .rr-rev");
                            if (replyContentPar.find(".teaser-content").length > 0) {
                                replyContent = replyContentPar.find(".teaser-content").html() + replyContentPar.find(".complete-content").html();
                            }
                            else {
                                replyContent = replyContentPar.html();
                            }
                            $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.POST_COMMENT }, function (result) { });
                            $.post("/Ajax/User/AutoSendMail.ashx", { idComment: $(".reviews").find(".review:first").attr("value"), replyContent: replyContent, commentType: $("#commentType").val(), objectid: $("#objectID").val(), parentComment: 0 }, function (result) { });
                        }
                        processinghide();
                    },
                    error: ServiceFailed
                });
            }
            //            }
            //            else {
            //                alert("Xin chờ 30 giây để tiếp tục đăng bình luận.");
            //            }
        }

    });
    $(document).on('click', ".rr-submit", function () {
        //        if (timerCheck) {
        //            timerCheck = 0;
        //            intervalTimerCheck = setInterval(CheckTimer, 30000);
        if ($(this).parents(".popup-review").length <= 0) {
            if (!$("#userID").val()) {
                $.colorbox({
                    //                iframe: true,
                    //                scrolling: false,
                    //                innerWidth: '90%',
                    //                innerHeight: '90%',
                    transition: "none",
                    inline: true,
                    fixed: true,
                    href: "#aregister"
                });
                return false;
            }
            else {
                processing();
                var p = $(this).parents(".review");
                if (p.find("textarea").val() != "") {
                    $.ajax({
                        url: "/Ajax/User/PostComment.ashx",
                        dataType: "jsonp",
                        data: {
                            rate0: 0,
                            txtComment: p.find("textarea").val(),
                            txtCommentBad: "",
                            rate1: 0,
                            rate2: 0,
                            rate3: 0,
                            rate4: 0,
                            commentType: $("#commentType").val(),
                            productid: $("#objectID").val(),
                            idComment: p.attr("value")
                        },
                        success: function (result) {
                            //                            timerCheck = 0;
                            //                            intervalTimerCheck = setInterval(CheckTimer, 30000);
                            $(".rr-textarea").parents(".rr-reply-form").hide();
                            var a = p.find(".rr-comments .rr-comment:last");
                            if (a.length) {
                                a.after(result);
                            }
                            else {
                                p.find(".rr-comments:first").prepend(result);
                            }
                            CheckUserType();
                            newsComment();
                            resetHeight();
                            var url = location.href.split('#')[0] + "#" + p.attr("value");
                            var replyContent = "";
                            var replyContentPar = $(".reviews").find(".review:first .rr-rev");
                            if (replyContentPar.find(".teaser-content").length > 0) {
                                replyContent = replyContentPar.find(".teaser-content").html() + replyContentPar.find(".complete-content").html();
                            }
                            else {
                                replyContent = replyContentPar.html();
                            }
                            $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.REPLY_COMMENT, viewUrl: url }, function (result) { });
                            $.post("/Ajax/User/AutoSendMail.ashx", { parentComment: p.attr("value"), replyContent: replyContent }, function (result) { });
                            processinghide();
                        },
                        error: ServiceFailed
                    });
                }
                else {
                    alert("Bạn chưa nhập bình luận.");
                }
            }
        }
        else {
            if ($(".popup-review textarea").val().trim() != "") {
                $.ajax({
                    url: "/Ajax/User/PostComment.ashx",
                    dataType: "jsonp",
                    data: {
                        rate0: 0,
                        txtComment: $(".popup-review textarea").val(),
                        txtCommentBad: "",
                        rate1: 0,
                        rate2: 0,
                        rate3: 0,
                        rate4: 0,
                        commentType: $("#commentType").val(),
                        productid: $("#objectID").val(),
                        editID: $("#editCommentID").val(),
                        currentPage: $(".rr-pagination span.rr-current").attr("data-page")
                    },
                    success: function (result) {
                        $.colorbox.close();
                        $(".rr-pagination > span[data-page='" + result + "']").click();
                        $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.EDIT_COMMENT, viewUrl: location.href + "#" + $("#editCommentID").val() }, function (result) { });
                    },
                    error: ServiceFailed
                });
            } else {
                alert("Bạn chưa nhập bình luận.");
            }
        }
        //        }
        //        else {
        //            alert("Xin chờ 30 giây để tiếp tục đăng bình luận.");
        //        }
    });
    $(document).on("click", ".more-re-review-show", function () {
        var replyDiv = $(this).parents(".rr-comments");
        $.ajax({
            url: "/Ajax/User/ShowAllReplyComment.ashx",
            dataType: "jsonp",
            data: { objectid: $("#objectID").val(),
                commentType: $("#commentType").val(),
                parentComment: replyDiv.parents(".review").attr("value"),
                isShowMore: true
                //                pageIndex: 1,
                //                pageSize:0,
                //                idUser: $("#userID").val()
                //long IDObject, int IDType, int IDComment, int PageIndex, int PageSize
            },
            success: function (result) {
                replyDiv.find(".rr-comment").remove();
                replyDiv.find(".more-re-review-show").before(result);
                replyDiv.slideDown("slow");
                replyDiv.find(".more-re-review-show").hide();
                //                replyDiv.find(".more-re-review-show").addClass("more-re-review-hide");
                //                replyDiv.find(".more-re-review-hide").removeClass("more-re-review-show");
                //                replyDiv.find(".more-re-review-hide").html("Hiện 2 bình luận trả lời gần nhất");
                ShowRemoveButtonProductDetail();
                resetHeight();
            },
            error: ServiceFailed
        });
    });
    $(document).on("click", ".more-re-review-hide", function () {
        var replyDiv = $(this).parents(".rr-comments");
        $.ajax({
            url: "/Ajax/User/ShowAllReplyComment.ashx",
            dataType: "jsonp",
            data: { objectid: $("#objectID").val(),
                commentType: $("#commentType").val(),
                parentComment: replyDiv.parents(".review").attr("value"),
                isShowMore: false
            },
            success: function (result) {
                replyDiv.find(".rr-comment").remove();
                replyDiv.find(".more-re-review-hide").before(result);
                replyDiv.find(".more-re-review-hide").remove();
                ShowRemoveButtonProductDetail();
                resetHeight();
            },
            error: ServiceFailed
        });
    });
    $(".rr-filter").click(function () {
        processing();
        var sortExpr = "";
        if ($("#ddlSortComment").length > 0) {
            sortExpr = $("#ddlSortComment").val();
        }
        $("#hdfVote").val($(this).attr("data-filter"));
        $.ajax({
            url: "/Ajax/User/PagingComment.ashx",
            dataType: "jsonp",
            data: { productid: $("#objectID").val(),
                commentType: $("#commentType").val(),
                vote: $("#hdfVote").val(),
                pageIndex: 1,
                idUser: $("#userID").val(),
                sortExpr: sortExpr
            },
            success: function (result) {
                $(".ListComments").html(result);
                pagingEffect($(".rr-pagination"));
                $(".rr-textarea").parents(".rr-reply-form").hide();
                checkCommentType();
                newsComment();
                CheckUserType();
                countComment();
                resetHeight();
                $('html, body').animate({
                    scrollTop: $(".ListComments").offset().top - 100
                }, 500);
                processinghide();
            },
            error: ServiceFailed
        });
    });

    $(document).on("click", ".ro-ex-more", function () {
        //        $(this).parent().find(".ro-ex-hide").removeClass("ro-ex-hide");
        //        $(this).hide();
        var p = $(this).parents(".ListCommentsEx");
        expertPageIndex++;
        $(this).remove();
        $.ajax({
            url: "/Ajax/User/PagingComment.ashx",
            dataType: "jsonp",
            data: { productid: $("#objectID").val(),
                commentType: $("#commentType").val(),
                pageIndex: expertPageIndex,
                vote: -1,
                idUser: $("#userID").val()
            },
            success: function (result) {
                p.append(result);
            },
            error: ServiceFailed
        });
    });

    //paging effect
    $(".rr-pagination").each(function () {
        pagingEffect($(this));
    });

    $("#ddlSortComment").chosen({
        width: "150px",
        height: "100%",
        disable_search_threshold: 10
    });
    //sort comment
    $("#ddlSortComment").change(function () {
        var page1 = $(".rr-pagination > span[data-page='1']");
        if (page1.length > 0) {
            page1.click();
        }
    });
    ///edit comment
    ShowRemoveButtonProductDetail();
    $(document).on('click', ".rr-delete .delete_button", function () {
        var id = 0;
        processing();
        if ($(this).parents(".rr-comment").length <= 0) {
            id = $(this).parents(".review").attr("value");
            $.post("/Ajax/User/EditComment.ashx", { commentID: id,
                pageIndex: $(".rr-pagination span.rr-current").attr("data-page"),
                idUser: $("#userID").val()
            },
              function (result) {
                  var html = "<div class='popup-review'>" + result + "</div>";
                  $.colorbox({
                      scrolling: true,
                      preloading: false,
                      fixed: true,
                      innerWidth: '50%',
                      html: html
                  });
                  if ($(".ratable").length > 0) { rate($(".ratable")); }
                  processinghide();

              }, 'jsonp');
        }
        else {
            id = $(this).parents(".rr-comment").attr("value");
            $.post("/Ajax/User/EditComment.ashx", { commentID: id,
                pageIndex: $(".rr-pagination span.rr-current").attr("data-page"),
                idUser: $("#userID").val()
            },
              function (result) {
                  var html = "<div class='popup-review'>" + result + "</div>";
                  $.colorbox({
                      scrolling: true,
                      preloading: false,
                      fixed: true,
                      innerWidth: '50%',
                      html: html
                  });
                  if ($(".ratable").length > 0) { rate($(".ratable")); }
                  processinghide();
              }, 'jsonp');
        }
    });

    //pagging
    $(document).on("click", ".rr-pagination > span", function () {
        var idUser = ($("#currentUserID").length > 0) ? $("#currentUserID").val() : $("#userID").val();
        var sortExpr = "";
        if ($("#ddlSortComment").length > 0) {
            sortExpr = $("#ddlSortComment").val();
        }
        addCurrentPage($(this));
        var p = $(this).parents(".ListComments");
        //        pagingEffect($(".rr-pagination"));
        $.ajax({
            url: "/Ajax/User/PagingComment.ashx",
            dataType: "jsonp",
            data: { productid: $("#objectID").val(),
                commentType: $("#commentType").val(),
                pageIndex: $(".rr-current").attr('data-page'),
                vote: $("#hdfVote").val(),
                idUser: idUser,
                sortExpr: sortExpr
            },
            success: function (result) {
                p.html(result);
                if (parseInt($("#objectID").val()) > 0) {
                    ShowRemoveButtonProductDetail();
                }
                else {
                    ShowRemoveButtonUserDetail();
                }
                pagingEffect(p.find(".rr-pagination"));
                $(".rr-textarea").parents(".rr-reply-form").hide();
                checkCommentType();
                newsComment();
                CheckUserType();
                countComment();
                resetHeight();
                if ($(".reviews").length > 0) {
                    $('html, body').animate({
                        scrollTop: $(".ListComments").offset().top - 100
                    }, 500);
                }
            },
            error: ServiceFailed
        });
    });
    $(".reduce-price").click(function () {
        var html = "<div class='popup-review'></div>";
        $.colorbox({
            href: "#ReducePricePopup",
            inline: true,
            fixed: true,
            innerHeight: '270px',
            innerWidth: '480px'
        });
    });
    var commentState = readCookie('commentState');
    writeCookie('commentState', "", -1);
    if (commentState != "") {
        var arr = JSON.parse(commentState);
        if (arr.length > 8) {
            if ($("#objectID").val() == arr[8]) {
                $("#rate0").find("span.star[data-star='" + arr[0] + "']").click();
                $("#rate1").find("span.star[data-star='" + arr[3] + "']").click();
                $("#rate2").find("span.star[data-star='" + arr[4] + "']").click();
                $("#rate3").find("span.star[data-star='" + arr[5] + "']").click();
                $("#rate4").find("span.star[data-star='" + arr[6] + "']").click();
                minWords1 = arr[2];
                $("textarea.rf-review-body.text1").val(arr[1]);
                $("textarea.rf-review-body.text1").keyup();
            }
            //            $(".rr-pagination .rr-page[data-page='" + arr[7] + "']").click();
        }
    }
    resetHeight();
    //    $("#ddlSortComment").val("PublicDate");
});
//end document-ready
//before unload
window.onbeforeunload = function () {
    arrComState = [$("#rate0").attr('data-value'),
                    $(".text1").val(),
                    minWords1,
                    $("#rate1").attr('data-value'),
                    $("#rate2").attr('data-value'),
                    $("#rate3").attr('data-value'),
                    $("#rate4").attr('data-value'),
                    $(".rr-pagination .rr-current").attr('data-page'), $("#objectID").val()];
    writeCookie('commentState', JSON.stringify(arrComState), 1);
}
//paging
function addCurrentPage(s) {
    if (s.hasClass("rr-next")) {
        var cur = $(".rr-current");
        cur.removeClass("rr-current");
        cur.next().addClass("rr-current");
    }
    else if (s.hasClass("rr-prev")) {
        var cur = $(".rr-current");
        cur.removeClass("rr-current");
        cur.prev().addClass("rr-current");
    }
    else {
        $(".rr-pagination span").removeClass("rr-current");
        s.addClass("rr-current");
    }
    $(".rr-page").not(".shadow").addClass("shadow");
    $(".rr-current").removeClass("shadow");
}
function pagingEffect(s) {
    if (s.children(".rr-current").next().hasClass("rr-next")) {
        s.children(".rr-next").hide();
    }
    else s.children(".rr-next").show();
    if (s.children(".rr-current").prev().hasClass("rr-prev")) {
        s.children(".rr-prev").hide();
    }
    else s.children(".rr-prev").show();
    checkPaging(s);
}
//show popup viewmore
$(document).on('click', ".rev-link", function () {
    var id = $(this).attr("value");
    $.post("/Ajax/User/PopupComment.ashx", { commentid: id },
              function (result) {
                  var html = "<div class='popup-review'>" + result + "</div>";
                  $.colorbox({
                      scrolling: true,
                      preloading: false,
                      fixed: true,
                      innerHeight: '80%',
                      innerWidth: '60%',
                      html: html
                  });
                  $(".rr-textarea").parents(".rr-reply-form").hide();
              }, 'jsonp');
});
//show more rating
$(document).on('click', ".review-more-toggle", function () {
    var more = $(this).siblings(".rr-more-data");
    if (more.is(':hidden')) {
        more.slideDown();
    } else more.slideUp();
    resetHeight();
});
//show more Noidung
$(document).on('click', ".rr-read-more .link", function () {
    var par = $(this).parents(".rr-read-more");
    if (par.hasClass("view-more")) {
        par.siblings(".complete-content").show();
        par.removeClass("view-more");
    }
    else {
        par.siblings(".complete-content").hide();
        par.addClass("view-more");
    }
});

//Show more v1

$(document).ready(function () {
    var s = true;
    $('.more').click(function () {
        // $('.ah').text = "Xem thêm.";
        //$(this).val("Xem thêm");
        if (s == true) {
            $('.complete-content').toggle('normal');
            $(this).html("Thu gọn");
            s = false;

        }
        else {
            $('.complete-content').toggle('normal');
            $(this).html("Xem thêm");
            s = true;
        }
    })
});


////////////9
function ShowTextareaReply(s) {
    var text = "";
    if (s.hasClass("reply-to")) {
        var name = s.parents(".comment-name").find(".rr-comment-username").html();
        text = "@" + name + ":";
    }
    var more = s.parents(".clearfix").find(".rr-textarea");
    more.val(text);
    if (more.parents(".rr-reply-form").is(':hidden')) {
        more.parents(".rr-reply-form").slideDown();
    } else {
        more.parents(".rr-reply-form").slideUp();
        //        more.siblings(".rr-submit").hide();
    }
    more.siblings(".rr-submit").hide();
    resetHeight();
}
///////////8
//function ShowMoreRating(s) {
//    var more = s.siblings(".rr-more-data");
//    if (more.is(':hidden')) {
//        more.slideDown();
//    } else more.slideUp();
//}
///////////////7
function newsComment() {
    if ($("#commentType").val() == "2") {
        $(".review-rating").remove();
        $("#rr-overview").remove();
        $(".rf-attr-ratings").remove();
        //$(".rr-helpful").remove();
    }
}
///////////6
function countComment() {
    var count = $(".reviews > .review").length;
    if (count > 0) {
        //        $(".showing").html("Hiển thị 1 - " + count + " trong " + count + " đánh giá");
        //        $(".rr-showing").html("Hiển thị 1 - " + count + " trong " + count + " đánh giá");
        $(".reviews .noDataMes").remove();
        $(".sort-wrap").show();
    }
    else {
        $(".sort-wrap").hide();
        $(".showing").html("");
        $(".rr-showing").html("");
        switch ($("#commentType").val()) {
            case "1":
                $(".reviews").html("<div class='noDataMes'><a href='#ratingcheck'>Bạn hãy là người đầu tiên đánh giá sản phẩm này.</a></div>");
                break;
            case "2":
                $(".reviews").html("<div class='noDataMes'>Chưa có bình luận nào.</div>");
                break;
            case "3":
                $(".reviews").html("<div class='noDataMes'>Chưa có đánh giá nào.</div>");
                break;
        }
    }
}
//////////////5
function CheckUserType() {
    if ($("#userType").val() == "1") {
        $(".share-face").show();
    }
    else {
        $(".share-face").hide();
    }
}
function ShowRemoveButtonUserDetail() {
    if ($("#currentUserID").val() !== $("#sessionUserID").val()) {
        $(".remove-comment").remove();
        $(".delete_button").remove();
    }
}
function ShowRemoveButtonProductDetail() {
    $(".rr-delete").remove();
    var editButton = "<span class=\"rr-delete\"><a class=\"delete_button\" title=\"Sửa đánh giá này.\"><i class=\"i icon-edit\"></i></a></span>";
    var a = $("#userID").val();
    $(".rr-username").each(function () {
        var pa = $(this).parents(".review-right");
        var user = pa.find(".rr-actions:first");
        if ($(this).attr("user-value") == a) {
            user.append(editButton);
        }
        else {
            user.find(".rr-delete").remove();
        }
    });
    $(".comment-name").each(function () {
        if ($(this).attr("user-value") == a) {
            $(this).find(".rr-actions").append(editButton);
        }
        else {
            $(this).find(".rr-delete").remove();
        }
    });
}
/////////////4
$(document).on('click', ".rr-vote", function () {
    if (!$("#userID").val()) {
        $.colorbox({
            transition: "none",
            inline: true,
            fixed: true,
            href: "#aregister"
        });
        return false;
    }
    else {
        var selector = $(this);
        var idComment = selector.attr("value");
        if (selector.hasClass("up")) {
            $.ajax({
                url: "/Ajax/User/VoteYesNo.ashx",
                dataType: "jsonp",
                data: { vote: 'true',
                    userID: $("#userID").val(),
                    idComment: idComment
                },
                success: function (result) {
                    selector.siblings(".rr-count").remove();
                    selector.after(result.TotalVoteGood);
                    selector.siblings(".down").after(result.TotalVoteBad);
                    var url = location.href.split('#')[0] + "#" + idComment;
                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.VOTE_YES_NO, viewUrl: url }, function (result) { });
                    $.post("/Ajax/User/AutoSendMail.ashx", { idComment: idComment, vote: 'true', isVote: 'true' }, function (result) { });
                },
                error: ServiceFailed
            });
        }
        if (selector.hasClass("down")) {
            $.ajax({
                url: "/Ajax/User/VoteYesNo.ashx",
                dataType: "jsonp",
                data: { vote: 'false',
                    userID: $("#userID").val(),
                    idComment: idComment
                },
                success: function (result) {
                    selector.siblings(".rr-count").remove();
                    selector.after(result.TotalVoteBad);
                    selector.siblings(".up").after(result.TotalVoteGood);
                    var url = location.href.split('#')[0] + "#" + idComment;
                    $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.VOTE_YES_NO, viewUrl: url }, function (result) { });
                    $.post("/Ajax/User/AutoSendMail.ashx", { idComment: idComment, vote: 'false', isVote: 'true' }, function (result) { });
                },
                error: ServiceFailed
            });
        }
    }
});
////////////3
function editName() {
    if ($(".rf-display-name").is(':hidden')) {
        $(".rf-display-name").slideDown();
    } else {
        $(".rf-display-name").slideUp();
    }
    resetHeight();
}
function SaveDisplayName() {
    processing();
    var displayName = $(".rf-dn-input").val().trim();
    if (displayName != "") {
        $.ajax({
            url: "/Ajax/User/SaveUserDisplayName.ashx",
            dataType: "jsonp",
            data: { displayName: displayName },
            success: function (result) {
                $("b.rf-name").html(result.Name);
                $(".rf-display-name").slideUp();
                resetHeight();
                $.post("/Ajax/User/LogUserComment.ashx", { action: UserCommentLogType.CHANGE_DISPLAYNAME }, function (result) { });
                processinghide();
            },
            error: ServiceFailed
        });
    }
    else alert("Bạn phải nhập tên hiển thị.");
}
/////////////2
function ResetControl(s) {
    s.find(".rating-bar").css("width", "0%");
    s.find(".rf-error-sec").css("display", "none");
    s.find(".rf-attr-ratings").css("display", "none");
    s.find("i").css("display", "none");
    s.find(".rf-review-body").val("");
    if ($("#commentType").val() == "1" || $("#commentType").val() == "3") {
        drawRate($("#rate0"), "large", "");
        drawRate($("#rate1"), "", "");
        drawRate($("#rate2"), "", "");
        drawRate($("#rate3"), "", "");
        drawRate($("#rate4"), "", "");
        drawRate($("#rate6"), "", "");
        s.find(".ratable").attr("data-value", "0");
    }
    s.html("<div class=\"rf-message\" id=\"ratingcheck\">Cảm ơn đóng góp của bạn.</div>");
    resetHeight();
}
////////////1
function validateRate(s) {
    if (s.length > 0) {
        if (s.attr("data-value") == 0) {
            s.parents(".rf-wrap").find(".icon-yes").css('display', 'none');
            s.parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
            return error1;
        } else {
            s.parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
            s.parents(".rf-wrap").find(".icon-yes").css('display', 'inline-block');
            return "";
        }
    }
    else {
        return "";
    }
}
//validate text news
function validateText(s, w) {
    if (w >= 15) {
        s.parents(".rf-wrap").find(".icon-yes").css('display', 'none');
        s.parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
        return error2;
    } else if (w > 0) {
        s.parents(".rf-wrap").find(".icon-yes").css('display', 'none');
        s.parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
        return error3;
    } else {
        s.parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
        s.parents(".rf-wrap").find(".icon-yes").css('display', 'inline-block');
        return "";
    }
}
//validate text product
function validateTextarea() {
    if (minWords1 >= 15 && minWords2 >= 15) {
        $(".rf-review-body").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
        $(".rf-review-body").parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
        //        return error4;
        return error2;
    }
    else {
        if (minWords1 > 0 && minWords1 < 15) {
            $(".text1").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
            $(".text1").parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
            return error3;
        }
        if (minWords2 > 0 && minWords2 < 15) {
            $(".text2").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
            $(".text2").parents(".rf-wrap").find(".icon-no").addClass("rf-missed");
            return error3;
        }
    }
    $(".rf-review-body").parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
    $(".rf-review-body").parents(".rf-wrap").find(".icon-yes").css('display', 'inline-block');
    if (minWords1 >= 15) {
        $(".text1").parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
        $(".text1").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
    }
    if (minWords2 >= 15) {
        $(".text2").parents(".rf-wrap").find(".icon-no").removeClass("rf-missed");
        $(".text2").parents(".rf-wrap").find(".icon-yes").css('display', 'none');
    }
    return "";
}

function validateReview() {
    //    if ($("#commentType").val() == 1) {
    //        htmlError = validateRate($("#rate0")) + validateTextarea();
    //    }
    //    if ($("#commentType").val() == 2) {
    $("textarea").keyup();
    htmlError = validateRate($("#rate0")) + validateText($(".text1"), minWords1);
    //    }
    if (htmlError != "") {
        $(".rf-error-sec").css('display', 'block');
        $(".rf-error-sec").html(htmlError);
        return false;
    } else {
        $(".rf-error-sec").css('display', 'none');
        return true;
    }
    resetHeight();
}
function resetHeight() {
    var a = $(".overview").height() + $(".bar").height() + $("#box-compare").height() + $("#box-relate").height() + $("#box-tech").height() + $("#box-review").height() + $("#box-map").height();
    //    if (isUp) {
    //        a += v;
    //     }
    $('.detail-left').css("min-height", a + "px");
}

function scrollToReview() {
    var x = $(location).attr('hash');
    if (x != "") {
        x = x.split('#')[1];
        if ($("div[value='" + x + "']").length > 0) {
            $('html, body').animate({
                scrollTop: $("div[value='" + x + "']").offset().top - 70
            }, 500);
        }
        else {
            if ($("#box-review").length > 0) {
                $('html, body').animate({
                    scrollTop: $("#box-review").offset().top - 70
                }, 500);
            }
        }
    }
}
function checkCommentType() {
    if ($("#commentType").val() == "2") {
        $("#rr-overview").remove();
        //        $(".rf-attr-ratings").remove();
    }
    $(".rf-attr-ratings").remove();
}
function checkPaging(s) {
    //    if (s.children("span").length <= 3) {
    if (s.children("span").length <= 1) {
        s.hide();
    }
    else {
        s.show();
    }
}
function processing() {
    var modal = $('<div />');
    modal.addClass("modal");
    $('body').append(modal);
    var loading = $(".loading");
    loading.show();
    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
    loading.css({ top: top, left: left });
}
function processinghide() {
    $(".loading").hide();
    $(".modal").hide().removeClass("modal");
}