﻿
function getpagindex() {
    var loca = location.href;
    var rl = loca.split("").reverse().join("");
    var resuft = "";
    if(loca.indexOf("/trang-") != -1)
    {
        resuft = rl.substring(rl.indexOf('.')+1,rl.indexOf('-'));
        resuft = resuft.split("").reverse().join("");
    }
    else resuft = "";
    return resuft;
}
function getfilter() {
    var loca = location.href;
    var rl = loca.split("").reverse().join("");
    var resuft = "";
    if(loca.indexOf("/filter-") != -1)
    {
        resuft = rl.substring(rl.indexOf('.')+1,rl.indexOf('-'));
        resuft = resuft.split("").reverse().join("");
    }
    else resuft = "";
    return resuft;
}
function getsort() {
    var loca = location.href;
    var rl = loca.split("").reverse().join("");
    var resuft = "";
    if(loca.indexOf("/sort-") != -1)
    {
        resuft = rl.substring(rl.indexOf('.')+1,rl.indexOf('-'));
        resuft = resuft.split("").reverse().join("");
    }
    else resuft = "";
    return resuft;
}

function calllstproductdetailcatefilte(catid) {
    $.ajax({
        type: "GET",
        url: "/Public/ProductFilters.ashx",
        dataType: "jsonp",
        data: {
            cat: catid,
            filter: getfilter(),
            sort: getsort(),
            key: "",
            PageIndex: getpagindex()
        },
        success: function (data) {

            lstproductfilte(data);
        },
        error: ServiceFailed// When Service call fails
    });
}
// Start load data veiewprodata category
function lstproductfilte(result) {
    var strVar = "";
    var strvarv = "";
    var jsondatacate = eval(result.catehead);
    var jsondatapro = eval(result.prodata);
    for (var k = 0; k < jsondatapro.length; k++) {
        strVar += "<div class=\"item\"><div class=\"item-inner\"><div class=\"img-large\"><a target=\"_self\" title=\"" + jsondatapro[k].Name + "\" href=\"" + jsondatapro[k].Url + "\"><img alt=\"" + jsondatapro[k].Name + "\" title=\"" + jsondatapro[k].Name + "\" src=\"" + jsondatapro[k].ImageThumb + "\" onerror=\"LoadImage(this,'" + jsondatapro[k].ImageThumbErr + "')\"/></a></div>";
        strVar += "<h3><a target=\"_self\" title=\"" + jsondatapro[k].Name + "\" href=\"" + jsondatapro[k].Url + "\">"+jsondatapro[k].Name + "</a></h3><p class=\"price group\"><strong>Giá:</strong><span><label>" + jsondatapro[k].PriceVN + "</label><em>Giá thấp nhất</em><em>" + jsondatapro[k].Des + "</em></span></p>";
        if (jsondatacate.length > 0) {
            strVar += "<p><strong>" + jsondatacate[0].NameProperties + ":</strong>" + jsondatapro[k].Ex1 + "</p>";
            if(jsondatacate.length > 1)
            {
                strVar += "<p><strong>" + jsondatacate[1].NameProperties + ":</strong>" + jsondatapro[k].Ex2 + "</p>";
                if(jsondatacate.length > 2)
                {
                    strVar += "<p><strong>" + jsondatacate[2].NameProperties + ":</strong>" + jsondatapro[k].Ex3 + "</p>";
                    if(jsondatacate.length>3)
                    {
                        strVar += "<p><strong>" + jsondatacate[3].NameProperties + ":</strong>" + jsondatapro[k].Ex4 + "</p>";
                    }
                }
            }
        }
        strVar += "<div class=\"item-detail-compare group\"><div class=\"item-detail\"><label><input type=\"checkbox\" class=\"select-compare\" name=\"check" + jsondatapro[k].ID + "\" />So sánh</label></div>";
        strVar += "<div class=\"item-compare\"><a  target=\"_self\" title=\"" + jsondatapro[k].Name + "\" href=\"" + jsondatapro[k].Url + "\" class=\"move\">"+jsondatapro[k].View+"</a></div></div></div></div>";
        if ((k + 1) % 3 == 0) {
            strVar += "<div style=\"clear:both;\"></div>";
        }
    }
    $("#viewlistpro").html(strVar);
//    $('.col').click(function () {

//        $('.col-active').removeClass('col-active');
//        $(this).addClass("col-active");
//    });
    $('.select-compare').change(function () {
        //   debugger
        var name = $(this).attr("name");
        if (!$(this).is(':checked')) {
            $(".select-compare[name=" + name + "]").parents(".list-row").removeClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").removeClass("item-inner-active");
            $(".select-compare[name=" + name + "]").prop('checked', false);
        } else {
            $(".select-compare[name=" + name + "]").prop('checked', true);
            $(".select-compare[name=" + name + "]").parents(".list-row").addClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").addClass("item-inner-active");
        }
        updateCounter();
    });
    //count checkbox
    function updateCounter() {

        var len = $(".select-compare:checked").length;

        if (len > 0) {

            $('.bottom-fix').addClass("bottom-fix-show");
            $(".item-selected em").text(len / 2);

        } else {
            $('.bottom-fix').removeClass("bottom-fix-show");

        }

    }

}
function callproductfilterSort(catid,sort_AdSc) {    
    $.ajax({
        type: "GET",
        url: "/Public/ProductFilters.ashx",
        dataType: "jsonp",
        data: {
            cat: catid,
            sortImporter: sort_AdSc,
            filter: "",
            sort: "",
            key: "",
            PageIndex: "1"
        },
        success: function (data) {
            productfilterSort(data);
        },
        error: ServiceFailed// When Service call fails
    });
}
// Start load data veiewprodatasort category
function productfilterSort(result) {
    var strVar = "";
    var jsondatapro = eval(result);
    for (var j = 0; j < jsondatapro.length; j++) {
            strVar += "<div class=\"list-row group\"><div class=\"row-col1\"><div class=\"col1-inner group\"><div class=\"img-product\"><a target=\"_self\" title=\"" + jsondatapro[j].Name + "\" href=\"" + jsondatapro[j].Url + "\"><img alt=\"" + jsondatapro[j].Name + "\" title=\"" + jsondatapro[j].Name + "\" src=\"" + jsondatapro[j].ImageThumb + "\" onerror=\"LoadImage(this,'" + jsondatapro[j].ImageThumbErr + "')\"/></a></div>";
            strVar += "<h3><a target=\"_self\" title=\"" + jsondatapro[j].Name + "\" href=\"" + jsondatapro[j].Url + "\">" + jsondatapro[j].Name + "</a></h3> <div class=\"detail-compare\"><div class=\"compare\"><label>";
            strVar += "<input type=\"checkbox\" class=\"select-compare\" name=\"check" + (j + 1) + "\"/>Chọn So sánh</label></div></div></div></div><div class=\"row-col2\">";
            strVar += "<div class=\"row-col2-col row-col\"><div class=\"row-col-inner\"><p class=\"price price-color\">" + jsondatapro[j].PriceVN + "</p><p>Giá thấp nhất</p><p>" + jsondatapro[j].Des + "</p><a  target=\"_self\" title=\"" + jsondatapro[j].Name + "\" href=\"" + jsondatapro[j].Url + "\" class=\"move\">Tới nơi bán </a></div></div>";
            strVar += "<div class=\"row-col2-col row-col\"><div class=\"row-col-inner\"><p class=\"price\">" + jsondatapro[j].Ex1 + "</p><p></p></div></div><div class=\"row-col2-col col2-col4 row-col\"><div class=\"row-col-inner\"><p class=\"price\">" + jsondatapro[j].Ex2 + "</p><p></p></div></div>";
            strVar += "<div class=\"row-col2-col col2-col4 row-col\"><div class=\"row-col-inner\"><p class=\"price\">" + jsondatapro[j].Ex3 + "</p><p></p></div></div><div class=\"row-col2-col col2-col5 row-col\"><div class=\"row-col-inner\"><p class=\"price\">" + jsondatapro[j].Ex4 + "</p><p></p></div></div>";
            strVar += "</div></div>";
        }
        $("#jsonviewsort").html(strVar);
    $('.select-compare').change(function () {
        //   debugger
        var name = $(this).attr("name");
        if (!$(this).is(':checked')) {
            $(".select-compare[name=" + name + "]").parents(".list-row").removeClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").removeClass("item-inner-active");
            $(".select-compare[name=" + name + "]").prop('checked', false);
        } else {
            $(".select-compare[name=" + name + "]").prop('checked', true);
            $(".select-compare[name=" + name + "]").parents(".list-row").addClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").addClass("item-inner-active");
        }
        updateCounter();
    });
    //count checkbox
    function updateCounter() {
        var len = $(".select-compare:checked").length;
        if (len > 0) {
            $('.bottom-fix').addClass("bottom-fix-show");
            $(".item-selected em").text(len / 2);
        } 
        else { $('.bottom-fix').removeClass("bottom-fix-show"); }
    }
}

function callmenutopdir() {
    $.ajax({
        type: "GET",
        url: "/Public/MenutopCate.ashx",
        dataType: "jsonp",
        data:null,
        success: function (data) {
            menutopdir(data);
        },
        error: ServiceFailed// When Service call fails
    });
}

function menutopdir(result) {
    var strVar = "";
    var jsondatapro = eval(result);
    strVar += "<ul>";
    for (var j = 0; j < jsondatapro.length; j++) {
            strVar += " <li><a href= '"+jsondatapro[j].URLCat +"' title='"+jsondatapro[j].Name+"'>"+jsondatapro[j].Name+"</a></li>";
        }
    strVar += "</ul>";
   $("#sidr").html(strVar);
}
