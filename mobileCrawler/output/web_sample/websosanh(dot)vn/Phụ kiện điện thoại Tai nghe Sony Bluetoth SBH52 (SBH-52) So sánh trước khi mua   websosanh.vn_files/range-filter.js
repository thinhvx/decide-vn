﻿function sliderangewss(s, minVal, maxVal, valF, valT) {
    //screen size slider
    var valFrom = parseFloat(valF);
    var valTo = parseFloat(valT);
    var min = parseFloat(minVal);
    var max = parseFloat(maxVal);
    s.find(".slide-bar").slider({
        range: true,
        min: (min), //parseFloat($("#valBottom").attr('min')),//('2'),//Min
        max: (max), //parseFloat($("#valTop").attr('max')),//('7'),//Max
        step: 1,
        values: [valFrom, valTo],
        slide: function (event, ui) {
            valFrom = ui.values[0];
            valTo = ui.values[1];
            s.find(".valBottom").val(valFrom);
            s.find(".valTop").val(valTo);
        },
        change: function (event, ui) {
            var val;
            valFrom = ui.values[0];
            valTo = ui.values[1];
            s.find(".valBottom").val(valFrom);
            s.find(".valTop").val(valTo);
            var filter = $(this).parents(".filter-box").attr('data-fid');

            addfilter(filter, valFrom, valTo);
        }

    });
    s.find(".valBottom").val(s.find(".slide-bar").slider("values", 0));
    s.find(".valTop").val(s.find(".slide-bar").slider("values", 1));
    s.find(".valBottom").change(function () {
        valFrom = isNaN(parseInt($(this).val())) ? min : parseInt($(this).val());
        if (valFrom > valTo) {
            valFrom = valTo;
        }
        s.find(".slide-bar").slider("values", [valFrom, valTo]);
    });
    s.find(".valTop").change(function () {
        valTo = isNaN(parseInt($(this).val())) ? max : parseInt($(this).val());
        if (valTo < valFrom) {
            valTo = valFrom;
        }
        s.find(".slide-bar").slider("values", [valFrom, valTo]);
    });
}

function addfilter1(filter, valFrom, valTo) {
    var url = window.location.href;
    var urlitems = url.split('.h')[0].split('/');
    var filted = false;
    urlitems = urlitems.filter(function (n) { return n.indexOf("trang"); });
    for (var i = urlitems.length - 1; i >= 0; i--) {
        if (urlitems[i].indexOf("filter") == 0) {
            var filteritems = urlitems[i].split('\-')[1].split(',');
            for (var j = 0; j < filteritems.length; j++) {
                if (filteritems[j].indexOf(filter + "_") >= 0) {
                    filteritems[j] = filter + "_" + valFrom + "~" + valTo;
                    filted = true;
                    break;
                }
            }
            if (!filted) {
                filteritems.push(filter + "_" + valFrom + "~" + valTo);
            }
            urlitems[i] = "filter-" + filteritems.join(',');
            filted = true;
            break;
        }
    }
    if (!filted) {
        urlitems.push("filter-" + filter + "_" + valFrom + "~" + valTo);
    }

    load(urlitems.join('/') + ".htm");

    //$.ajax({
    //    url: '',
    //    type: 'POST',
    //    data: filters,
    //    dataType: 'json',
    //    success: function (data) {
    //        alert("results")
    //    },
    //    error: function () {
    //        alert("error filter");
    //    }
    //});
}
function addfilter2(filter, valFrom, valTo) {
    var url = window.location.href;
    var urlitems = url.split('.h')[0].split('/');
    var filted = false;
    urlitems = urlitems.filter(function (n) { return n.indexOf("trang"); });
    for (var i = urlitems.length - 1; i >= 0; i--) {
        if (urlitems[i].indexOf("sort") == 0) {
            urlitems.splice(i, 1);
        } else {
            if (urlitems[i].indexOf("__sort") >= 0) {
                urlitems[i] = urlitems[i].substring(0, urlitems[i].indexOf("__sort"));
            }
            if (urlitems[i].indexOf("filter") == 0) {
                var filteritems = urlitems[i].split('\-')[1].split(',');
                for (var j = 0; j < filteritems.length; j++) {
                    if (filteritems[j].indexOf(filter + "_") >= 0) {
                        filteritems[j] = filter + "_" + valFrom + "~" + valTo;
                        filted = true;
                        break;
                    }
                }
                if (!filted) {
                    filteritems.push(filter + "_" + valFrom + "~" + valTo);
                }
                urlitems[i] = "filter-" + filteritems.join(',');
                filted = true;
                break;
            }
        }
    }
    if (!filted) {
        urlitems.push("filter-" + filter + "_" + valFrom + "~" + valTo);
    }

    load(urlitems.join('/') + ".htm");

    //$.ajax({
    //    url: '',
    //    type: 'POST',
    //    data: filters,
    //    dataType: 'json',
    //    success: function (data) {
    //        alert("results")
    //    },
    //    error: function () {
    //        alert("error filter");
    //    }
    //});
}

function addfilter(filter, valFrom, valTo) {
    var url = window.location.href;
    var urlitems = url.split('.h')[0].split('/');
    var filted = false;
    var sortStr = "";    
    urlitems = urlitems.filter(function (n) { return n.indexOf("trang"); });
    for (var i = urlitems.length - 1; i >= 0; i--) {
        //sortStr = "";
        if (urlitems[i].indexOf("sort") == 0) {
            filted = false;
            sortStr = urlitems[i].substring(urlitems[i].indexOf("sort"), urlitems[i].length);
            urlitems.splice(i, 1);
            break;
        } else {
            if (urlitems[i].indexOf("__sort") >= 0) {
                sortStr = urlitems[i].substring(urlitems[i].indexOf("__sort"), urlitems[i].length);
                urlitems[i] = urlitems[i].replace(sortStr, "");
            }
            if (urlitems[i].indexOf("filter") == 0) {
                var filteritems = urlitems[i].split('\-')[1].split(',');
                for (var j = 0; j < filteritems.length; j++) {
                    if (filteritems[j].indexOf(filter + "_") >= 0) {
                        filteritems[j] = filter + "_" + valFrom + "~" + valTo;
                        filted = true;
                        break;
                    }
                }
                if (!filted) {
                    filteritems.push(filter + "_" + valFrom + "~" + valTo);
                }
                urlitems[i] = "filter-" + filteritems.join(',');
                filted = true;
                urlitems[i] += sortStr;
                break;
            }
        }
    }
    if (!filted) {
        if (sortStr != "") {
            urlitems.push("filter-" + filter + "_" + valFrom + "~" + valTo + "__" + sortStr);
        } else {
            urlitems.push("filter-" + filter + "_" + valFrom + "~" + valTo);
        }
    }

    load(urlitems.join('/') + ".htm");

    //$.ajax({
    //    url: '',
    //    type: 'POST',
    //    data: filters,
    //    dataType: 'json',
    //    success: function (data) {
    //        alert("results")
    //    },
    //    error: function () {
    //        alert("error filter");
    //    }
    //});
}

function removefilter(v) {
    //    var filters = $.grep(filters, function (value) {
    //        return value.split('_')[0] != v;
    //    });
    //    $("#results").find("div[id='" + v + "']").remove();ftt
    //    addfilter();
}

$(document).ready(function () {
    $(".range-filter").each(function () {
            sliderangewss($(this), $(this).attr("data-min"), $(this).attr("data-max"), $(this).attr("data-from"), $(this).attr("data-to")); //data-min="2" data-max="7" data-from="3" data-to="5"
    });
    //multi select
    if ($(".multi-select").length > 0) {
        $(".multi-select").each(function () {
            var a = $(this).attr('value').split(',');
            $(this).val(a);
        });
    }
    $(".soft-select").chosen({
        width: "100%",
        height: "100%",
        disable_search_threshold: 10
    }).change(function () {
        load($(this).val());
    });
    $(".multi-select").chosen({
        width: "100%",
        height: "100%",
        disable_search_threshold: 10
    }).change(function () {
        var oldvalues = [];
        var newvalues = [];
        oldvalues = $(this).attr('value').split(',');
        oldvalues = oldvalues.filter(function (n) { return n != ""; });
        if ($(this).val() != null) {
            newvalues = $(this).val();
        }
        var addArray = newvalues.filter(function (el) {
            return oldvalues.indexOf(el) < 0;
        });
        var removeArray = oldvalues.filter(function (el) {
            return newvalues.indexOf(el) < 0;
        });
        if (addArray.length > 0) {
            var addurl = $(this).find("option[value='" + addArray[0] + "']").attr("url");
            load(addurl);
            return;
        }
        if (removeArray.length > 0) {
            var removeurl = $(this).find("option[value='" + removeArray[0] + "']").attr("disableurl");
            load(removeurl);
            return;
        }
    });
    //end multi select

    //price range filter
    $(document).on("click", ".choose-price-inner .btn-compare", function () {
        var parent = $(this).parents(".filter-box");
        var minValue = parseInt(parent.find(".min-price").val().replace(/\./g, ""));
        var maxValue = parseInt(parent.find(".max-price").val().replace(/\./g, ""));
        if (isNaN(minValue)) {
            parent.find(".min-price").focus();
            return false;
        }
        if (isNaN(maxValue)) {
            parent.find(".max-price").focus();
            return false;
        }
        if (minValue < maxValue) {
            addfilter(parent.attr('data-fid'), minValue, maxValue);
            return true;
        }
        else {
            $(".submit-price").append("<p style='color: Red;'>Giá tìm kiếm nhập chưa chuẩn xác</p>");
            return false;
        }
    });
    //end price range filter
});





