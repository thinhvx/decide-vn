﻿$(document).ready(function () {
    drawRate($("#rate0"), "large", "");
    drawRate($("#rate1"), "", "");
    drawRate($("#rate2"), "", "");
    drawRate($("#rate3"), "", "");
    drawRate($("#rate4"), "", "");
    drawRate($("#rate6"), "", "");
    drawRate($("#rate7"), "", "");
    drawRateColor($("#rate7"), $("#rate7").attr("data-value"));
    drawRateColor($("#rate0"), $("#rate0").attr("data-value"));
    drawRateColor($("#rate1"), $("#rate1").attr("data-value"));
    drawRateColor($("#rate2"), $("#rate2").attr("data-value"));
    drawRateColor($("#rate3"), $("#rate3").attr("data-value"));
    drawRateColor($("#rate4"), $("#rate4").attr("data-value"));
    drawRateColor($("#rate6"), $("#rate6").attr("data-value"));
    if ($(".set-rating").length > 0) {
        $(".set-rating").each(function() {
            drawRate($(this), "", "");
            drawRateColor($(this), $(this).attr("data-value"));
        });
    }
});

function rate(s) {
    var fr = 0;
    //$(".ratable span.star").each(function () {
    s.find("span.star").each(function () {
        $(this).mouseover(function () {
            drawRateColor($(this).parent(), parseInt($(this).attr('data-star')));
            $(this).siblings(".rating-bar").css('width', parseInt($(this).attr('data-star')) * 20 + '%');
        });
        $(this).click(function () {
            $(this).addClass("active").delay(1000).queue(function () {
                $(this).removeClass("active").dequeue();
            });
            $(this).siblings(".rating-bar").css('width', parseInt($(this).attr('data-star')) * 20 + '%');
            fr = parseInt($(this).attr('data-star'));
            fr = (fr < 0) ? 0 : fr;
            fr = (fr > 5) ? 5 : fr;
            $(this).parents(".ratable").attr("data-value", fr);
//            if (arrComState) {
//                arrComState[0] = fr;
//            }
            drawRateColor($(this).parent(), parseInt($(this).attr('data-star')));
            if ($(this).parents(".ratable").attr("id") == "rate6") {
                //                drawRateColor($("#rate0"), $(this).attr("data-star"));
                //                $("#rate0").attr("data-value", $(this).attr("data-star"));
                //                validateRate($("#rate0"));
                $("#rate0").find("span.star[data-star='" + $(this).attr("data-star") + "']").click();
                validateRate($("#rate0"));
            }
            if (s.attr("id") == 'rate0') {
                validateRate(s);
            }
        });
    });
    //$(".ratable span.spacer").each(function () {
    s.find("span.spacer").each(function () {
        $(this).mouseover(function () {
            $(this).siblings(".rating-bar").css('width', (parseInt($(this).attr('data-star'))) * 20 + '%');
        });
    });
    //$(".ratable .rating-container").mouseleave(function () {
    s.find(".rating-container").mouseleave(function () {
        var f = parseInt($(this).parents(".ratable").attr("data-value"));
        drawRateColor($(this), f);
        $(this).children(".rating-bar").css('width', f * 20 + '%');
    });
}
function drawRate(s, cl, c) {
    var html = "<div class='rating-container stars " + cl + "'>";
    html += "<div class='rating-bar" + c + "'></div>";
    html += "<div class='rating-bar-back js-stared'></div>";
    for (var i = 1; i <= 5; i++) {
        html += "<span class='star' data-star='" + i + "'>";
        html += "<span class='spr star-icon'></span>";
        html += "<span class='tl'></span>";
        html += "<span class='tr'></span>";
        html += "<span class='bl'></span>";
        html += "<span class='br'></span>";
        html += "</span>";
        if (i < 5) {
            html += "<span class='spacer' data-star='" + i + "'></span>";
        }
    }
    html += "</div>";
    s.html(html);
    if (s.hasClass("ratable")) {
        rate(s);
    }
}
function drawRateColor(s, val) {
    s.find(".rating-bar").removeClass("red");
    s.find(".rating-bar").removeClass("yellow");
    s.find(".rating-bar").removeClass("green");
    if (val <= 2) {
        s.find(".rating-bar").addClass("red");
    }
    else if (val <= 4) {
        s.find(".rating-bar").addClass("yellow");
    } else {
        s.find(".rating-bar").addClass("green");
    }
    s.find(".rating-bar").css('width', val * 20 + '%');
}