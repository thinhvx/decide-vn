﻿
$(document).ready(function () {

    $(".txt-search").focusin(function () {
        $(".search-solid").addClass("border-blue");
    });
    $(".txt-search").focusout(function () {
        $(".border-blue").removeClass("border-blue");
    });
    if ($('.bxslider').length > 0) {
        $('.bxslider').bxSlider({
            auto: true
        });
    }
    var mywindow = $(window);
    var mypos = mywindow.scrollTop();
    var up = false;
    var newscroll;
    mywindow.scroll(function () {
        newscroll = mywindow.scrollTop();
        if (newscroll > mypos && !up) {
            $('.header').stop().addClass("header-fix");

            //  $('.lading').css('padding-top','60px');
            up = !up;
        } else if (newscroll < mypos && up) {
            $('.header').stop().removeClass("header-fix");
            //  $('.lading').css('padding-top', '0px');
            up = !up;
        }
        mypos = newscroll;
    });

    $(".menu-icon a").click(function (evn) {
        evn.preventDefault();
        $('html,body').scrollTo(this.hash, this.hash);
    });
    var aChildren = $(".menu-icon li").children(); // find the a children of the list items
    var aArray = []; // create the empty aArray
    for (var i = 0; i < aChildren.length; i++) {
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values

    $(window).scroll(function () {
        var windowPos = $(window).scrollTop() + 10; // get the offset of the window from the top of page
        var windowHeight = $(window).height(); // get the height of the window
        var docHeight = $(document).height();
        for (var i = 0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top; // get the offset of the div from the top of page
            var divHeight = $(theID).height(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("nav-active");
            } else {
                $("a[href='" + theID + "']").removeClass("nav-active");
            }
        }
        if (windowPos + windowHeight == docHeight) {
            if (!$(".menu-icon li:last-child a").hasClass("nav-active")) {
                var navActiveCurrent = $(".nav-active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                $(".menu-icon li:last-child a").addClass("nav-active");
            }
        }
    });

    //RESIZE
    function doResize() {
        var w = $('.widow-width').width();
        if (w == 1200) {
            $('.test1').css('background-color', 'yellow');
            $('.filter').removeClass("filter-fix");
        }
        if (w > 992) {
            $('.filter').removeClass("filter-fix"); ;

        }
        if (w <= 992) {
            $('.filter').addClass("filter-fix");

            $('.test1').css('background-color', 'blue');
            $('.show-filter').click(function () {
                if ($('.filter').is(':visible')) {
                    $('.filter').addClass("filter-fix");
                }
                else {

                    $('.filter').removeClass("filter-fix");
                }

            });

            $('.show-filter').click(function (event) {
                event.stopPropagation();
            });
            $('.filter').click(function (event) {

                event.stopPropagation();
            });
        }
        if (w <= 480) {
            $('.test1').css('background-color', 'lime');
            $(".search-cate input[type='text']").focus(function () {


                $(".search-cate").addClass("auto-search");
                //   $(".search-cate").css("width", "100%");

                $(this).focusout(function () {
                    $(".search-cate").removeClass("auto-search");
                });

            });

        }
        if (w == 480) {
            $('.test1').css('background-color', 'gray');
        }
        if (w >= 1200) {
            $('.tab-pop').hide();
        }

    };

    doResize();
    $(window).on('resize', doResize);






    $('html').click(function () {
        if ($(window).width() <= 992) {
            $('.filter').addClass("filter-fix");
        }
    });

    $('div').on({
        mouseenter: function () {
            $('.active-li').removeClass('active-li');
            $(this).addClass('active-li');
        },
        mouseleave: function () {

        }
    }, ".nav-cate > li");


    //SHOW MENU-Home
    $('.nav').click(function () {
        var tab = $('.pop-menu');
        if ($(tab).is(':visible')) {
            $(tab).hide();
        }
        else {
            $(tab).show();
            //            $('.pop-menu').load('menu/menu.txt');
        }
    });
    $('html').click(function () {
        $('.pop-menu').hide();
    });
    $('.nav').click(function (event) {
        event.stopPropagation();
    });
    $('.pop-menu').click(function (event) {
        event.stopPropagation();
    });

    $('.pop-menu').on('click', '.close1', function (event) {
        $('.pop-menu').hide();
    });
    //    $('.ul-menu-res').load('menu/menu_res.txt');

    $('.nav-touch').click(function () {
        if ($(this).hasClass('nav-touch-show')) {
            $('.menu-res').removeClass('menu-res-show');
            $(this).removeClass('nav-touch-show');
            $('.page').css({ "height": "auto", "overflow": "visible" })
            $('.login-fix').removeClass('login-fix')
        }
        else {

            $('.menu-res').addClass('menu-res-show');
            $(this).addClass('nav-touch-show');
            var h = $('.menu-res').height();
            $('.page').css({ "height": h + "px", "overflow": "hidden" })
            if ($(window).width() < 460) {
                $('.login').addClass('login-fix');
            }
            $("html, body").animate({ scrollTop: 0 }, 0);

        }
    });

    $('html').click(function () {
        $('.menu-res').removeClass('menu-res-show');
        $('.nav-touch').removeClass('nav-touch-show');
        $('.page').css({ "height": "auto", "overflow": "visible" })
        $('.login-fix').removeClass('login-fix')
    });
    $('.nav-touch').click(function (event) {
        event.stopPropagation();
    });
    $('.menu-res').click(function (event) {
        event.stopPropagation();
    });


    //Show Profile Login

    $('.loginshow').click(function () {
        var login_ul = $('.loginshow ul');
        if ($(login_ul).is(':visible')) {
            $(login_ul).hide();
        }
        else {
            $(login_ul).show();

        }
    });
    $('html').click(function () {
        $('.loginshow ul').hide();
    });

    //    if ($('.tabs_default').length > 0) {
    //        $('.tabs_default').tabslet();
    //    }
    $('.list-radio').find('li').hide();
    $('.list-radio').find('li:lt(5)').show();
    $('.reall-all').click(function () {
        var str = $(this).attr("title");
        if (str == "show") {

            $(this).siblings('ul').find('li').show();
            $(this).attr("title", "hide");
            $(this).html("<em>Thu gọn <i class='fa fa-caret-up'></i></em>");
        }
        else {
            $(this).siblings('ul').find('li').hide();
            $(this).siblings('ul').find('li:lt(5)').show();

            $(this).attr("title", "show");

            $(this).html("<em>Xem thêm <i class='fa fa-caret-right'></i></em>");

        }
    });

    $(".list-radio").each(function () {
        var c = $(this).find('li').length;
        if (c <= 5) {

            $(this).parent().find('.reall-all').hide();
        }
        var inde = 0;
        var inde1 = 0;

        $(this).find('input').each(function () {
            if (this.checked) {
                inde1 = inde;
            }
            inde++;
        });
        if (inde1 >= 5) {
            $(this).find('li').show();
            var span = $(this).parent().find('.reall-all');
            $(span).attr("title", "hide");

            $(span).html("<em>Thu gọn <i class='fa fa-caret-up'></i></em>");
        }
    });

    /// <reference path="../css1/cate.css" />

    $('.login').hover(function () {
        $('.toptip').fadeOut();

    });

    ///tab

    $('#tab-map').css({ "height": "0px", "overflow": "hidden" });

    $('#tab-map').addClass();
    $('#tab-tech').fadeOut();
    $('.tabs .tab-bar li').click(function () {

        var title = $(this).find('a').attr('title');
        $('.tab-content').hide();
        $("#" + title).show();
        if (title == "tab-map") {
            $("#" + title).css({ "height": "400px", "overflow": "hidden", "padding": "10px 0px 10px 210px" });
        }

        $(".tabs .tab-bar li").removeClass("active");
        $(this).addClass("active");

    });
    /// <reference path="../css1/bootstrap-theme.min.css" />

    $(window).resize(function () {
        $('.wrap-col-center').outerWidth($('.wrap-col').width() - ($('.wrap-col-left').outerWidth() + $('.wrap-col-right').outerWidth() + 2));

        //show tab_map like popup

        $('.show-popup-map').click(function () {

            if ($('.widow-width').width() <= 768) {


                $('.tab-map').addClass("tap-map-popup");
                // alert($(window).height());

                $('#tab-map').addClass("fix-height-map");
                // $('#tab-map').css({ "height": "auto !important", "overflow": "hidden" });
                // $("#tab-map").css("height", "700px !important");

                $('#map_canvas').height($(window).height() - 210);

                $('.close-map').click(function () {

                    $('.tab-map').removeClass("tap-map-popup");

                    $(".tab-bar li").removeClass("active");
                    $(".tab-bar li").first().addClass("active");
                    $("#tab-map").hide();
                    $("#tab-store").show();
                });
            }
        });

    }).trigger('resize');



    $('.totop').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 800);
        return false;
    });
    //cap
    $('.read-more-cate').click(function () {
        $('.list-cate-filter').toggleClass("list-cate-filter-all");
    });

});






