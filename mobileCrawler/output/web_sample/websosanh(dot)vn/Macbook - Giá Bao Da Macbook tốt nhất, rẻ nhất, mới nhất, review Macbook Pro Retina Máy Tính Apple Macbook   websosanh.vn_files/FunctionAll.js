﻿
function autocompletsearch(divid, UrlKeyword) {
    function log(message) {
        $("#" + divid).scrollTop(0);
    }
    $("#" + divid).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "GET",
                url: UrlKeyword,
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term,
                    key: encodeURIComponent($("#" + divid).val())
                },
                contentType: "application/json; charset=utf-8",
                dataType: "jsonp",
                jsonp: "callback",
                success: function (data) {
                    response($.map(data.autos, function (item) {
                        return {
                            label: item.KeyName,
                            value: item.KeyName
                        }
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            if (divid !== "companyName") {
                $("#" + divid).val(ui.item.value);
                search(divid);
            }
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

}
//thu: ko tu dong search khi chon 1 tuy chon goi y
function autocompletsearchnotenter(divid, UrlKeyword) {
    function log(message) {
        $("#" + divid).scrollTop(0);
    }
    $("#" + divid).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "GET",
                url: UrlKeyword,
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term,
                    key: encodeURIComponent($("#" + divid).val())
                },
                contentType: "application/json; charset=utf-8",
                dataType: "jsonp",
                jsonp: "callback",
                success: function (data) {
                    response($.map(data.autos, function (item) {
                        return {
                            label: item.KeyName,
                            value: item.KeyName
                        }
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            if (divid !== "companyName") {
                $("#" + divid).val(ui.item.value);
                //search(divid);
            }
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

}
var catidcompare = "";
var keypro = "";
$(document).ready(function () {
    catidcompare = $("#hdfCatid").val();
    keypro = $("#prsearch").val();
    if ($("#results .tagl-item").length > 0) {
        $("#results").show();
    } else {
        $("#results").hide();        
    }
});

$(function () {
    var Urlkey = "/Public/AutoCompletes.ashx";
    var UrlkeyCat = "/Public/LsProductByCatid.ashx";
    autocompletsearch("qtsearch", Urlkey);
    autocompletsearch("qtsearchdir", Urlkey);
    autosearchComparePro("prsearch", UrlkeyCat);
    ////auto complete for company name (CompanyRegister.ascx)
    var UrlKeyCompanyName = "/Ajax/Company/AutoCompleteCompanyName.ashx";
    autocompletsearch("companyName", UrlKeyCompanyName);
    ///auto complete for products manage (ProductsManage.ascx)
    autocompletsearchnotenter("product-name-input", Urlkey);
});


/* Start Login*/
function calllogin() {
    $.ajax({
        url: "/Public/LoginUserWeb.ashx",
        dataType: "jsonp",
        data: {
            email: encodeURIComponent($("#edit-name").val()),
            pass: encodeURIComponent($("#edit-pass").val())
        },
        success: function (data) {
            //            callprohotSucceeded(data);
            alert('Đăng nhập thành công');
        },
        error: ServiceFailed// When Service call fails
    });
}
/* End Login*/

function ServiceFailed(xhr) {
    //    alert(xhr.responseText);
    if (xhr.responseText) {
        var err = xhr.responseText;
        if (err)
            error(err);
        else
            error({ Message: "Unknown server error." })
    }
    return;
}

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}
String.prototype.replaceAll = function (
                                                strTarget, // The substring you want to replace
                                                strSubString // The string you want to replace in.
                                                ) {
    var strText = this;
    var intIndexOfMatch = strText.indexOf(strTarget);
    // Keep looping while an instance of the target string
    // still exists in the string.
    while (intIndexOfMatch != -1) {
        // Relace out the current instance.
        strText = strText.replace(strTarget, strSubString)

        // Get the index of any next matching substring.
        intIndexOfMatch = strText.indexOf(strTarget);
    }
    // Return the updated string with ALL the target strings
    // replaced out with the new substring.
    return (strText);
}
/************************************************************/
/* Function IndexToCompare						            */
/* Get list Value and send to compare link                  */
/*                                                          */
/************************************************************/
function IndexToCompare() {
    var label = "";
    $(".detail-compare  input:checked").each(function () {
        label += $(this).attr('id') + "-";
    });
    label = label.substring(0, label.length - 1);
    var url = window.location.host + "/compare/" + label + ".htm";
    window.location.href = url;
    // window.open(url, '_blank');
}
/************************************************************/
/* Function SelectToCompare						            */
/* Send to compare link                                     */
/*                                                          */
/************************************************************/
function SelectToCompare(id) {
    var url = '';
    url += '/compare/';
    url += encodeURIComponent(id) + '.htm';
    location.href = url;
    return false;
}

function onEnter(evt, ndivid) {
    var keyCode = null;
    if (evt.which) {
        keyCode = evt.which;
    } else if (evt.keyCode) {
        keyCode = evt.keyCode;
    }
    if (13 == keyCode) {
        search(ndivid);
        return false;
    }
    return true;
}
function search(ndivid) {
    var key = document.getElementById(ndivid).value;
    key = key.trim();
    key = key.replaceAll('`', ' ');
    key = key.replaceAll('~', ' ');
    key = key.replaceAll('!', ' ');
    key = key.replaceAll('@', ' ');
    key = key.replaceAll('#', ' ');
    key = key.replaceAll('$', ' ');
    key = key.replaceAll('%', ' ');
    key = key.replaceAll('^', ' ');
    key = key.replaceAll('&', ' ');
    key = key.replaceAll('*', ' ');
    key = key.replaceAll('(', ' ');
    key = key.replaceAll(')', ' ');
    key = key.replaceAll('-', ' ');
    key = key.replaceAll('_', ' ');
    key = key.replaceAll('+', ' ');
    key = key.replaceAll('=', ' ');
    key = key.replaceAll('{', ' ');
    key = key.replaceAll('}', ' ');
    key = key.replaceAll('[', ' ');
    key = key.replaceAll(']', ' ');
    key = key.replaceAll('|', ' ');
    key = key.replaceAll('\\', ' ');
    key = key.replaceAll(':', ' ');
    key = key.replaceAll(';', ' ');
    key = key.replaceAll('\'', ' ');
    key = key.replaceAll('"', ' ');
    key = key.replaceAll('<', ' ');
    key = key.replaceAll('>', ' ');
    key = key.replaceAll(',', ' ');
    key = key.replaceAll('.', ' ');
    key = key.replaceAll('?', ' ');
    key = key.replaceAll('/', ' ');
    key = key.replaceAll("  ", " ");
    key = key.replaceAll(" ", "+");
    if (key.length > 0) {
        window.location.href = "/s/" + key + ".htm";
    }
    else {
        document.getElementById(ndivid).focus();
        document.getElementById(ndivid).Text = "Nhập nội dung tìm kiếm";
    }
}
function LoadImage(id, src) {
    var oImg = new Image;
    oImg.src = src;
    oImg.onload = function () { id.src = src; id.onerror = null; }
    oImg.onerror = function () { id.src = "/images/NoImages/no.jpg"; }
}

var product_url = window.location.href;
function share_facebook() { window.open('http://www.facebook.com/share.php?u=' + product_url); }
function share_twitter() { window.open('http://twitter.com/home?status=' + product_url); }
function share_google() { window.open('http://www.google.com/bookmarks/mark?op=edit&bkmk=' + product_url); }
function share_buzz() { window.open('http://bookmarks.yahoo.com/toolbar/savebm?opener=tb&u=' + product_url); }
function share_delicious() { window.open('http://www.delicious.com/save?url=' + product_url); }
function share_digg() { window.open('http://digg.com/submit?url=' + product_url); }
function share_zing() { window.open('http://link.apps.zing.vn/share?url=' + product_url); }
function showSub(groupId) {
    var check = document.getElementById(groupId).style.display;
    if (check == "none") {
        document.getElementById("icon_" + groupId).src = "/Images/Common/expand_on.gif";
        $("#" + groupId).slideDown("slow");
    } else {
        document.getElementById("icon_" + groupId).src = "/Images/Common/expand_off.gif";
        $("#" + groupId).slideUp("slow");
    }
}
function ShowInsertcompare() {
    var iduser = $("#hdfIDUser").val();
    var product_url = window.location.href;
    $(document).click(function (e) {
        if (e.target.className !== "Sharethis") {
            $('.Sharethis').removeClass("active");
        }
        else {
            if (!$('.Sharethis').hasClass("active")) { $('.Sharethis').addClass("active"); }
            else { $('.Sharethis').removeClass("active"); }
        }
    });
    $('.AddProduct').click(function () {
        if (!$('.CompareSearchidPro').hasClass("activeCompareSearch")) {
            $('.AddProduct').addClass("active");
            $('.CompareSearchidPro').addClass("activeCompareSearch");
        }
        else {
            $('.AddProduct').removeClass("active");
            $('.CompareSearchidPro').removeClass("activeCompareSearch");
        }
    });
    $('span.close').click(function () {
        $('.AddProduct').removeClass("active");
        $('.CompareSearchidPro').removeClass("activeCompareSearch");
    });
    var title = $("#hdftitle").val();
    $("#txttitle").val(title);
    $(document).on('click', "#btnsave", function () {
        var title = $("#txttitle").val();
        var contdetail = $("#txtnotes").val();
        if (!$("#hdfIDUser").val()) {
            var valuecompare = [title, contdetail, product_url]
            writeCookie('valueComment', JSON.stringify(valuecompare), 1);
            $.colorbox({
                transition: "none",
                inline: true,
                href: "#return-login"
            });
            writeCookie('comparelogin', "true", 1);
            return false;
        }
        else {
            Savelinkcompare(product_url, iduser, title, contdetail);
        }
    });
    $(document).on('click', ".SaveUser", function () {
        $.colorbox({
            transition: "none",
            inline: true,
            href: "#PopupSaveLink"
        });
    });
    $(window).load(function () {
        var a = readCookie('valueComment');
        writeCookie('valueComment', "", -1);
        if (a != "") {
            var arr = JSON.parse(a);
            if (arr.length > 9) {
                Savelinkcompare(arr[2], iduser, arr[0], arr[1]);
            }
        }
    });
}
function Savelinkcompare(urlcompare, iduser, title, contdetail) {
    $.post("/Ajax/User/SaveLinkCompareUser.ashx", { UrlCompare: urlcompare, Titlecompare: title, ContentCompare: contdetail, idUser: iduser }, function (result) { });
    alert('Bạn đã lưu thành công');
    refresh();
}
function autosearchComparePro(divid, UrlKeyword) {
    function log(message) {
        $("#" + divid).scrollTop(0);
    }
    $("#" + divid).autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "GET",
                url: UrlKeyword,
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    Catid: catidcompare,
                    Keywork: $("#" + divid).val()
                },
                contentType: "application/json; charset=utf-8",
                dataType: "jsonp",
                jsonp: "callback",
                success: function (data) {
                    response($.map(data, function (item) {
                        return {
                            icon: item.ImageUrlThumbNail,
                            label: item.Name,
                            value: item.Name,
                            id: item.Id
                        }
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            $("#" + divid).val(ui.item.value);
            $("#" + divid).attr("data-id", ui.item.id);
            AddProIdCompare(ui.item.id);
        },
        open: function () {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function () {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

}
function onEnterAddPro(evt, proid) {
    var keyCode = null;
    if (evt.which) {
        keyCode = evt.which;
    } else if (evt.keyCode) {
        keyCode = evt.keyCode;
    }
    if (13 == keyCode) {
        AddProIdCompare(proid);
        return false;
    }
    return true;
}
function AddProIdCompare(proid) {
    var idcat = $("#hdfCatid").val();
    var cookurl = readCookieCompare("Cat" + idcat);
    var arr = [];
    if (cookurl != "") {
        arr = cookurl.split('-');
        if (arr.indexOf(proid) < 0)
            arr.unshift(proid);
    }
    var label = arr.join('-');
    writeCookieCompare("Cat" + idcat, label, "");
    var urlp = "/compare/" + label + ".htm";
    load(urlp);
}
function getLsIdCompare(urlc) {
    var rl = urlc;
    var resuft = rl.substring(rl.indexOf('compare/'), rl.indexOf('.'));
    resuft = resuft.replace('compare/', "");
    return resuft;
}


function ShowAjaxComparePaging(p, psize, lstid) {
    $.ajax({
        url: "/Ajax/User/PagingCommentCompare.ashx",
        dataType: "jsonp",
        data: { productid: lstid,
            pageIndex: psize
        },
        success: function (result) {
            p.append(result);
        },
        error: ServiceFailed
    });
}


function formatCurrency(id, number) {
    document.getElementById(id).innerHTML = addCommas(number);
}

function addCommas(nStr) {
    nStr += ''; x = nStr.split(','); x1 = x[0]; x2 = ""; x2 = x.length > 1 ? ',' + x[1] : ''; var rgx = /(\d+)(\d{3})/; while (rgx.test(x1)) { x1 = x1.replace(rgx, '$1' + '.' + '$2'); } return x1 + x2;
}
function searchFilter() {
    var key = document.getElementById("qtsearch").value;
    var pmin = document.getElementById("qs_price").value;
    var pmax = document.getElementById("qs_price_to").value;
    var action = document.getElementById("filter_action").value;
    key = key.replaceAll("  ", " ");
    key = key.replaceAll(" ", "+");
    window.location.href = "/s/" + key + "/action-" + action + ",pmin_" + pmin + ",pmax_" + pmax + ".htm";
}

function create_clock() {
    var mydate = new Date();
    var year = mydate.getYear();
    if (year < 1000)
        year += 1900;
    var day = mydate.getDay();
    var month = mydate.getMonth();
    var daym = mydate.getDate();
    var hour = mydate.getHours();
    var minute = mydate.getMinutes();
    if (minute < 10)
        minute = "0" + minute;
    var second = mydate.getSeconds();
    if (second < 10)
        second = "0" + second;
    if (daym < 10)
        daym = "0" + daym;
    var dayarray = new Array("Ch&#7911; nh&#7853;t", "Th&#7913; hai", "Th&#7913; ba", "Th&#7913; t&#432;", "Th&#7913; n&#259;m", "Th&#7913; s&#225;u", "Th&#7913; b&#7849;y");
    var montharray = new Array("/01", "/02", "/03", "/04", "/05", "/06", "/07", "/08", "/09", "/10", "/11", "/12");
    document.getElementById("clock").innerHTML = dayarray[day] + ", " + daym + "" + montharray[month] + "/" + year + " | " + hour + ":" + minute + ":" + second;
}

function getCate() {
    var loca = location.href;
    var rl = loca.split("").reverse().join("");
    var resuft = rl.substring(rl.indexOf('.') + 1, rl.indexOf('-'));
    resuft = resuft.split("").reverse().join("");
    return resuft;
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}
//            cat: getCate(),
//        Cat: getUrlVars()["cat"],

// End load data category
//count checkbox

function menuclick() {
    $(document.body).click(function () {
        if ($('.menu').hasClass("show-nav")) {
            $('.menu').removeClass("show-nav");
        }
    });
    $('.close').click(function () {
        $('.menu').toggleClass("show-nav");
    });
    $('.menu-wrap').click(function () {
        $('.menu').toggleClass("show-nav");
    });
    $('.menu').click(function (event) {
        event.stopPropagation();
    });
    //menu-res
    $('.menu-res').click(function () {

        $('.menu').toggleClass("show-nav");
        if ($('div').hasClass('show-nav')) {
            $('.menu-res').addClass("menu-res-active");
        }
        else {
            $('.menu-res').removeClass("menu-res-active");
        }
    });
    $('.menu-res').click(function (event) {
        event.stopPropagation();
    });
    $('.nav > li').hover(function () {
        $(".active-li").removeClass("active-li");
        $(this).addClass("active-li");
    });
    $('.menuleftcate li').click(function () {
        $(".active").removeClass("active");
        $(this).addClass("active");
    });
}
// Start ....
//$(window).load(function() { divloading().close(); });
$(document).ready(function () {
    //code menu poup
    menuclick();
    scollcate();
    //click hidden colleft
    $('.arrows-pos .arrow').click(function () {
        $('.middle').toggleClass("hidden-left");
        $('.colleft').toggleClass("bg_grad");
        $('.colleft').toggleClass("shadow");
        //$('.list-header').width($('.colright').width());
    });
    //login
    $('.loginshow').click(function () {
        $(this).addClass("lgactive");
    });
    $(document.body).click(function () {
        $('.loginshow').removeClass("lgactive");
    });
    $('.loginshow').click(function (event) {
        event.stopPropagation();
    });

    //clicl col

    $(window).resize(function () {
        $('.list-fixed').width($('.colright').width());
    });

    $(window).scroll(function () {
        var st = $(window).scrollTop();
        if (st >= 720) {
            $(".fixed-top").addClass("fixed-top-fix");
            $(".breadCrumb").addClass("breadCrumb-fix");
            
        } else {
            $(".fixed-top").removeClass("fixed-top-fix");
            $(".breadCrumb").removeClass("breadCrumb-fix");
        }

        var a = $('.bar-fix li a.nav-active').text();

        $('.frame-bar-fix h3').html(a);
    });

    $('.frame-bar-fix').click(function () {
        $('.frame-bar-fix').toggleClass("show-ul");
    });

    $('.show-noiban').click(function (event) {
        $('.show-noiban').toggleClass("show-ul-noiban");
    });


    $(document.body).click(function () {
        if ($(window).width() < 900) {
            $('.show-find .hidden').hide();
        }
        $('.show-sort-item').hide();
    });

    $('.show-sort').click(function (event) {
        event.stopPropagation();
        $('.show-sort-item').show();
    });

    $('.show-find').click(function (event) {
        $('.show-find .hidden').show();
        event.stopPropagation();
    });

    var popup;
    var intervallogin;
    $('.fb_login_button').click(function (event) {
        var x = screen.width / 2 - 700 / 2;
        var y = screen.height / 2 - 600 / 2;
        popup = window.open("/Ajax/User/FaceBook.aspx?login=false", "popupWindow", "width=700, height=500, scrollbars=yes, left=" + x + ", top=" + y);
        intervallogin = setInterval(CheckLogin, 1000);
    });

    function CheckLogin() {
        if (popup.closed) {
            clearInterval(intervallogin);
            refresh();
        }
    }

    function refresh() {
        window.location.reload(true);
    }

    scolltop();
    var sort_AdSc;
    var catid = $("#idcat").val();//.attr("value");

    $('.col').click(function () {
        $(this).addClass("col-active sortDESC");
        if ($(this).hasClass("col-active sortASC")) {
            $(this).removeClass("col-active sortASC").addClass("col-active sortDESC");
            $('.gridnotjs').hide();
        } else if ($(this).hasClass("col-active sortDESC")) {
            $(this).removeClass('col-active sortDESC').addClass('sortASC');
            $('.gridnotjs').hide();
        }
        divloading();
    });
    $(".col").find('a').each(function () {
        var hrefurl = $(this).attr("href");
        var mangval = $("#valurl").val();
        var mangchuoi = [];
        if (mangval.length != 0) {
            mangchuoi = mangval.split(',');
            if (mangchuoi[1] == "1") {
                if (hrefurl.indexOf(mangchuoi[0]) > 0) {
                    $(this).parents(".col").addClass("col-active sortDESC");
                }
            }
            if (mangchuoi[1] == "2") {
                if (hrefurl.indexOf(mangchuoi[0]) > 0) {
                    $(this).parents(".col").addClass("col-active sortASC");
                }
            }
        }
    });

    $('.select-compare').change(function () {
        //   debugger
        var name = $(this).attr("name");
        if (!$(this).is(':checked')) {
            $(".select-compare[name=" + name + "]").parents(".list-row").removeClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").removeClass("item-inner-active");
            $(".select-compare[name=" + name + "]").prop('checked', false);
        } else {
            $(".select-compare[name=" + name + "]").prop('checked', true);
            $(".select-compare[name=" + name + "]").parents(".list-row").addClass("list-row-active");
            $(".select-compare[name=" + name + "]").parents(".item-inner").addClass("item-inner-active");
        }
        updateCounter($(this));
        //        linkToCompare();
    });

    function linkToCompare() {
        var label = "";
        var idcat = $("#hdicatparent").val();
        var cookurl = readCookieCompare("Cat" + idcat);
        if (cookurl != "") {
            label += cookurl + "-";
        }
        $(".detail-compare input:checked").each(function () {
            if (label.indexOf($(this).val()) >= 0)
                label += "";
            else
                label += $(this).val() + "-";
        });
        label = label.substring(0, label.length - 1);
        writeCookieCompare("Cat" + idcat, label, "");
        //        $.post("/Public/SaveSessionCheckProduct.ashx", { Cat: catid, productid: label }, function (result) { });
        var url = "/compare/" + label + ".htm";
        $('.view-item-selected').attr("href", url);

    }

    function updateCounter(s) {
        //    var iselect = $("#hditemselect").val();
        var len = $(".select-compare:checked").length;
        var idcat = $("#hdicatparent").val();
        var cookurl = readCookieCompare("Cat" + idcat);
        var countcook = 0;
        var arr = [];
        if (cookurl != "") {
            arr = cookurl.split('-');
            countcook = arr.length;
        }
        var icount;
        if (countcook == 0) {
            icount = parseInt(len);
            $(".select-compare:checked").each(function () {
                arr.push($(this).val());
            });
        } else {
            if (s.is(":checked")) {
                icount = parseInt(countcook) + 1;
                arr.push(s.val());
            } else {
                icount = parseInt(countcook) - 1;
                var newarr = jQuery.grep(arr, function (value) {
                    return value != s.val();
                });
                arr = newarr;
            }
        }
        var label = arr.join('-');
        writeCookieCompare("Cat" + idcat, label, "");
        var url = "/compare/" + label + ".htm";
        $('.view-item-selected').attr("href", url);
        if (icount > 0) {
            $('.bottom-fix').addClass("bottom-fix-show");
            $(".item-selected em").text(icount);
        } else {
            $('.bottom-fix').removeClass("bottom-fix-show");
        }
    }


    $('.view-item').hide();
    $('.switch-item').hide();
    $('.switch-table').click(function () {
        $(document).ready(function () {
            divloading();
            calllstproductdetailcatefilte(catid);
        });
        $(this).hide();
        $('.hidden-view').hide();
        $('.switch-item').show();
        $('.view-item').show();
        $('.colright').removeClass("colright1");
    });
    $('.switch-item').click(function () {
        $(this).hide();
        $('.view-item').hide();
        $('.hidden-view').show();
        $('.switch-table').show();
        $('.colright').addClass("colright1");
    });


    $('.clear').click(function () {
        $(".select-compare").prop('checked', false);
        clearss();
        updateCounter();
        $(".item-inner").removeClass("item-inner-active");
        $(".list-row").removeClass("list-row-active");
    });
    $('.content-detail img').addClass("img-res");

    //sub brcrm
    if ($(".breadCrumb li").length > 0) {
        $(document).on('mouseover', ".breadCrumb li", function () {
            $(".breadCrumb li").removeClass("subnav-show");
            $(this).addClass("subnav-show");
        });
        $(document).on('mouseleave', ".breadCrumb li", function () {
            var th = $(this);
            window.setTimeout(function () {
                th.removeClass('subnav-show');
            }, 1000);
        });
    }
});

function clearss() {
    var idcat = $("#hdicatparent").val();
    var cookurl = readCookieCompare("Cat" + idcat);
    writeCookieCompare("Cat" + idcat, cookurl, -1);
}
function Clearidpro(idPro, url) {
    var idcat = $("#hdfCatid").val();
    var cookurl = readCookieCompare("Cat" + idcat);
    var arr = [];
    if (cookurl != "")
        arr = cookurl.split('-');
    if (cookurl.indexOf(idPro) >= 0) {
        if (cookurl.indexOf(idPro) == 0)
            cookurl = cookurl.replace(idPro + "-", "");
        else {
            if (arr.indexOf(idPro) == arr.length - 1)
                cookurl = cookurl.replace("-" + idPro, "");
            else {
                cookurl = cookurl.replace(idPro, "");
                cookurl = cookurl.replace("--", "");
            }
        }
    }
    if (arr.length == 1)
        writeCookieCompare("Cat" + idcat, cookurl, -1);
    else
        writeCookieCompare("Cat" + idcat, cookurl, "");
    window.location.href = url;
}
function AccodionMobile() {
    $('.cate-box-item h3').click(function () {
        var windowWidth = $(window).width();
        if (windowWidth <= 880) {
            $('.cate-box-inner').slideUp();
            $('.active1').removeClass("active1");
            if ($(this).next().is(':visible')) {
                $(this).next().slideUp();

                $(this).removeClass("active1");
            }
            else {
                $(this).next().slideDown();
                $(this).addClass("active1");
            }
        }
    });
}
function showactive() {
//    var dataidcat = $("#idcat").attr("data-id");
//    if (dataidcat != "undefined") {
//        if (dataidcat.length > 0 && dataidcat != "-1") {
//            $(".nav > li").each(function () {
//                var dataliidcat = $(this).attr("data-id");
//                if (dataliidcat == dataidcat) {
//                    $(".nav > li").removeClass("active-li");
//                    $(this).addClass("active-li");
//                }
//            });
//        }
//        else {
//            $(".nav > li").removeClass("active-li");
//            $('.nav >li:first-child').addClass("active-li");
//        }
//    }
}
function scolltop() {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 500) {
            $('#scroll_top').fadeIn();
        } else {
            $('#scroll_top').fadeOut();
        }
    });
    $('#scroll_top').click(function () {
        $('body,html').animate({ scrollTop: 0 }, 800);
        return false;
    });
}
function scollcate() {
    var maxHeight = 0;
    var heighttop = $('.top').height();
    var heightsc = heighttop + $('.breadCrumb').height();
    $(window).scroll(function () {
        if ($(this).scrollTop() > heightsc) { $('.top').addClass("top-fix"); }
        else { $('.top').removeClass("top-fix"); }
    });
    $(".subcate-col").each(function () {
        maxHeight = $(this).height() > maxHeight ? $(this).height() : maxHeight;
    });
    $('.subcate').css("min-height", maxHeight + "px");
    var heightScreen = $(window).height() - 105;
    $('.subcate-left').css("height", heightScreen + "px");

    AccodionMobile();
    $(window).resize(function () {
        var windowWidth = $(window).width();
        if (windowWidth > 880) {
            $('.cate-box-inner').show();
        }
    });
}
function divloading() {
    var modal = $('<div />');
    modal.addClass("modal");
    $('body').append(modal);
    var loading = $(".loading");
    loading.show();
    var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
    var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
    loading.css({ top: top, left: left });
    setTimeout(divloadinghide, 4000);
}
function divloadinghide() {
    $(".loading").hide();
    $(".modal").hide().removeClass("modal");
}
function load(url) {
    //    divloading();
    //    window.history.pushState("", "", url);
    //    window.location.reload(true);
    window.location.href = url;
}
function refresh() {
    window.location.reload(true);
}
function colSlide() {
    for (var i = 0; i < 100; i++) {
        var maxHeight = 0;
        var selector = ".rows" + i;

        $(selector).each(function () {
            maxHeight = $(this).height() > maxHeight ? $(this).height() : maxHeight;
        });
        $(selector).css("min-height", maxHeight + "px");
    }
}

//cookie compareproduct
function writeCookieCompare(name, value, days) {
    var date, expires;
    if (days) {
        date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}
function readCookieCompare(name) {
    var i, c, ca, nameEQ = name + "=";
    ca = document.cookie.split(';');
    for (i = 0; i < ca.length; i++) {
        c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return '';
}
//end cookie

$(document).ready(function () {  
    $('.frame-radio-wrapper').find('.radio-wrapper').hide();
    $('.frame-radio-wrapper').find('.radio-wrapper:lt(5)').show();
    $('.show-all-radio span').click(function () {
        var str = $(this).attr("title");
        if (str == "show") {

            $(this).parent().siblings('div').find('.radio-wrapper').show();

            // $('.frame-radio-wrapper .radio-wrapper').show();
            $(this).attr("title", "hide");
            $(this).text("Thu gọn");
        }
        else {
            $(this).parent().siblings('div').find('.radio-wrapper').hide();
            $(this).parent().siblings('div').find('.radio-wrapper:lt(5)').show();

            $(this).attr("title", "show");
            $(this).text("Xem thêm");
        }
    });

    $(".frame-radio-wrapper").each(function () {
        var c = $(this).find('.radio-wrapper').length;
        if (c <= 5) {

            $(this).parent().find('.show-all-radio').hide();
        }
        var inde = 0;
        var inde1 = 0;

        $(this).find('input').each(function () {
            if (this.checked) {
                inde1 = inde;
            }
            inde++;
        });
        if (inde1 >= 5) {
            $(this).find('.radio-wrapper').show();
            var span = $(this).parent().find('.show-all-radio');
            $(span).find('span').attr("title", "hide");

            $(span).find('span').text("Thu gọn");
        }
    });
    var index = 0;
    var index1
    $('.frame-checkbox-wrapper input').each(function () {
        if (this.checked) {
            index1 = index;
        }
        index++;
    });
    if (index1 >= 5) {
        $('.show-all-checkbox span').hide();
    }
    else {
        var c1 = $('.frame-checkbox-wrapper ul li').length;
        if (c1 <= 5) {
            $('.show-all-checkbox span').hide();
        }
        else {
            $('.frame-checkbox-wrapper').find('li').hide();
            $('.frame-checkbox-wrapper').find('li:lt(5)').show();
            $('.show-all-checkbox span').click(function () {
                var str1 = $(this).attr("title");
                if (str1 == "show") {

                    $(this).parent().siblings('div').find('li').show();
                    $(this).attr("title", "hide");
                    $(this).text("Thu gọn");
                }
                else {
                    $(this).parent().siblings('div').find('li').hide();
                    $(this).parent().siblings('div').find('li:lt(5)').show();

                    $(this).attr("title", "show");
                    $(this).text("Xem thêm");
                }
            });
        }
    }

    var textSearch = $('.title-header h1').text();
    $('.box-search input[type="text"]').val(textSearch);
    $('.seach-sidr input[type="text"]').val(textSearch);



    var cateID = $('#idcat').attr('data-id');
    //  alert(cateID);
    if (cateID == "119") {
        $('.scroll-box-tech').find('a').text("Thông tin");
        $('.scroll1-box-tech').find('a').text("Thông tin");


    }
    var hightCollef = $('.colleft').height();
    $('.middle').css("min-height", +hightCollef + "px");

});


// New V1Product
function SelectChangePagesize(a)
{
    load($(a).val());
}

function onlyNumbers(evt) {
    var e = event || evt;
    var charCode = e.which || e.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && (charCode != 46 && charCode != 44 && charCode != 8))
        return false;
    return true;
}
function onlyNumbersPrice(b) {
    var Num = $(b).val();
    Num += '';
    Num = Num.toString().replace(/\./g, '').split(',');
    var x = Num[0].trim();
    var x1 = Num.length > 1 ? ',' + Num[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x))
        x = x.replace(rgx, '$1' + '.' + '$2');
    return x;
}

function ConvertNumbersPrice(b) {
    if (onlyNumbers() == false)
        return false;
    else {
        return $(b).val(onlyNumbersPrice(b));
    }
}
function ComparetwoinputPrice(a, b) {
    var min = parseInt($(a).val().toString().replace(/\./g, ''));
    var max = parseInt($(b).val().toString().replace(/\./g, ''));
    if (min < max)
        return true;
    else {
        $(".submit-price").append("<p>Giá tìm kiếm nhập chưa chuẩn xác</p>");
        return false;
    }
}
//Jquery UserComment
$(".reduce-price").click(function () {
    var html = "<div class='popup-review'></div>";
    $.colorbox({
        href: "#ReducePricePopup",
        inline: true,
        fixed: true,
        innerHeight: '270px',
        innerWidth: '480px'
    });
});