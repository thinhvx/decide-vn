__author__ = 'thinhvoxuan'

from scrapy.contracts import Contract
from scrapy.contracts.default import UrlContract
from mobileCrawler.item.book_nobita import get_tiki_class
import scrapy.http.request


class Test_Parse_TikiSpider(Contract):
    """
        @test_parse_tiki_spider
    """
    name = 'test_parse_tiki_spider'

    def post_process(self, output):
        for each in output:
            metaData = each.meta
            assert metaData['parent_url'] == 'http://tiki.vn/new-products/sach-truyen-tieng-viet.html'


class Test_Parse_Item_TikiSpider(Contract):
    """
        @test_parse_item_tiki_spider
    """
    name = 'test_parse_item_tiki_spider'

    def post_process(self, output):
        print output
        for item in output:
            for key, value in item.items():
                print key, value
                print
                print