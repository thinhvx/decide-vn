__author__ = 'thinhvoxuan'
from mobileCrawler.pipline.abstract_pipelines import AdjustPriceBook
from mobileCrawler.item.book_nobita import get_tiki_class

def mock1():
    TIKI = get_tiki_class()
    tiki = TIKI()
    tiki['old_price'] = '120'
    tiki['reg_price'] = '0'
    tiki['sale_off'] = '88'
    return tiki

def test_AdjustPriceBook():
    item = mock1()
    x = AdjustPriceBook().pip_item(item, None)
    assert x['reg_price'] == x['old_price']
