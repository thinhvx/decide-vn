__author__ = 'thinhvoxuan'
from twisted.internet import reactor
from scrapy.utils.project import get_project_settings
from scrapy.crawler import Crawler
from scrapy import signals
from scrapy.xlib.pydispatch import dispatcher
from mobileCrawler.spiders.nobita_spider import NobitaSpider
from mobileCrawler.spiders.tiki_spider import TikiSpider
from mobileCrawler.spiders.vinabook_spider import VinabookSpider
from scrapy import log
from datetime import datetime
SPIDER = "SPIDER"
CRAWLER = "CRAWLER"

class CrawlerProcessParallel(object):

    def __init__(self, isLog=False):
        self.crawler_queue = {}
        self.reactor = reactor
        self.start_finished = False
        self.item = []
        self.log = isLog
        dispatcher.connect(self.de_queue, signals.spider_closed)
        dispatcher.connect(self.add_item, signals.item_scraped)


    def add_spider(self, spider, override_settings = {}):
        settings = get_project_settings()
        for key in override_settings:
            settings.set(key, override_settings[key])
        crawler = Crawler(settings)
        crawler.configure()
        crawler.crawl(spider)
        self.en_queue(spider, crawler)

    def en_queue(self, spider, crawler):
        if spider.name not in self.crawler_queue:
            print 'enQueue: ', spider.name
            self.crawler_queue[spider.name] = {
                SPIDER : spider,
                CRAWLER : crawler
            }

    def de_queue(self, *a, **kawgs):
        spider_stop = kawgs['spider']
        if spider_stop.name in self.crawler_queue:
            print 'deQueue: ', spider_stop.name
            spider_stop = self.crawler_queue[spider_stop.name][SPIDER]
            crawler = self.crawler_queue[spider_stop.name][CRAWLER]
            today = datetime.now()
            file_name = spider_stop.name + "_stats_" +  today.strftime("%Y%m%d %H%M%S")
            with open(file_name, 'w') as f:
                stats = crawler.stats.get_stats()
                for each_property in stats:
                    f.write("'%s': %s,\n" %(each_property, stats[each_property]))
            del self.crawler_queue[spider_stop.name]
        if not self.crawler_queue:
            self.stop()

    def add_item(self, item):
        log.msg(item)
        self.item.append(item)

    def start(self):
        for spider_name in self.crawler_queue:
            print 'start_spider: ', spider_name
            crawler = self.crawler_queue[spider_name][CRAWLER]
            crawler.start()
        if self.crawler_queue:
            print 'run reactor'
            self.start_finished = True
            self.reactor.run()
            print 'DONE'
        else:
            self.finished()

    def stop(self):
        if self.start_finished:
            print 'stop reactor'
            self.reactor.stop()
            self.finished()

    def finished(self):
        pass

if __name__ == "__main__":
    log.start(loglevel=log.DEBUG)
    cpp = CrawlerProcessParallel()

    cpp.add_spider(NobitaSpider(), override_settings = {
        'DOWNLOAD_DELAY': 2,
        'DOWNLOADER_MIDDLEWARES': {}
    })

    # cpp.add_spider(TikiSpider(), override_settings = {
    #     'DOWNLOAD_DELAY': 0,
    #     'DOWNLOADER_MIDDLEWARES':{}
    # })

    # cpp.add_spider(VinabookSpider())
    cpp.start()