#!/usr/bin/python
# -*- coding: utf8 -*-
__author__ = 'thinhvoxuan'
import re
import math
from fuzzywuzzy import fuzz, utils, StringMatcher
from tokenizer.TokenizerWrapper import TokenizerWrapper
import rson
import json
from collections import Counter

tkwrapper = TokenizerWrapper()

class MergeProcess(object):
    def __init__(self, *args):
        self.listFile = args


def load_content(filename):
    json_data = open(filename)
    data = rson.loads(json_data.read())
    print 'open file: %s with length %d' % (filename, len(data))
    return data


def truncate_string(str):
    # return utils.full_process(str)
    return str


def call_fuzz(x, y):
    return fuzz.token_set_ratio(x, y)

def partial(x, y):
    return matching_rate_2(clean_string(x), clean_string(y))
    # longer = clean_string(x if len(x) > len(y) else y)
    # smaller = clean_string(x if len(x) <= len(y) else y)
    # int_x = [matching_rate_2(longer[i:len(smaller)+i], smaller) for i in range(0, len(longer) - len(smaller) + 1)]
    # return max(int_x + [0])



def gen_compare(book_name):
    def compare_function(item1, item2):
        p1 = partial(book_name.encode('utf-8'), item1['title'].encode('utf-8'))
        p2 = partial(book_name.encode('utf-8'), item2['title'].encode('utf-8'))
        if p1 > p2:
            return -1
        elif p1 < p2:
            return 1
        else:
            return 0

    return compare_function


def find_top_ten(data, book_name):
    compare_function = gen_compare(book_name)
    output = sorted(data, cmp=compare_function)

    print
    print '--', book_name.encode('utf-8'), '--'

    for e in output[:10]:
        print e['title'].encode('utf-8'), partial(book_name.encode('utf-8'), e['title'].encode('utf-8'))
    print '-----------------------------------------'
    return output[:10]


def test_function1():
    # case 1 SIEU KINH TE HOC HAI HUOC
    # http://nobita.vn/842/sieu-kinh-te-hoc-hai-huoc.html
    # http://tiki.vn/sieu-kinh-te-hoc-hai-huoc-tai-ban-2013-p72961.html

    book_name_1 = unicode('Siêu Kinh Tế Học Hài Hước (Tái Bản 2014)', 'utf-8')
    book_name_2 = u'Kinh Tế Học Hài Hước (Tái Bản 2014)'

    file1_content = load_content('../output/nobita_spider20141206 133728.json')
    result1 = find_top_ten(file1_content, book_name_1)
    result2 = find_top_ten(file1_content, book_name_2)

    file2_content = load_content('../output/tiki_spider20141206 173643.json')
    result3 = find_top_ten(file2_content, book_name_1)
    result4 = find_top_ten(file2_content, book_name_2)

    # write_file('result1', result1)
    # write_file('result2', result2)
    # write_file('result3', result3)
    # write_file('result4', result4)



def test_function2():
    #case 2 HARRY PORTER
    #
    #
    file2_content = load_content('../output/tiki_spider20141206 173643.json')
    file1_content = load_content('../output/nobita_spider20141206 133728.json')

    book_name_1 = clean_string(u'Harry Potter Và Phòng Chứa Bí Mật')
    result1 = find_top_ten(file1_content, book_name_1)
    result2 = find_top_ten(file2_content, book_name_1)

    # book_name_2 = u'Harry Potter Và Phòng Chứa Bí Mật'
    # result3 = find_top_ten(file1_content, book_name_2)
    # result4 = find_top_ten(file2_content, book_name_2)

    write_file('testcase2_result1', result1)
    write_file('testcase2_result2', result2)

def test_function3():
    #case 3 CHUA TE NHUNG CHIEC NHAN
    #
    #

    file2_content = load_content('../output/tiki_spider20141206 173643.json')
    file1_content = load_content('../output/nobita_spider20141206 133728.json')

    book_name_1 = unicode('Harry Potter Và Phòng Chứa Bí Mật ', 'utf-8')
    find_top_ten(file1_content, book_name_1)
    find_top_ten(file2_content, book_name_1)

    # book_name_2 = u'Harry Potter Và Phòng Chứa Bí Mật'
    # find_top_ten(file1_content, book_name_2)
    # find_top_ten(file2_content, book_name_2)


def token_count_string(string):
    global tkwrapper
    count_in_doc = Counter(tkwrapper.token_item(string))
    return count_in_doc


def append_global_count(string, global_count):
    count_in_doc = token_count_string(string)
    for (key, value) in count_in_doc.items():
        if key not in global_count:
            global_count[key] = {
                'freq': value,
                'docs': 1
            }
        else:
            global_count[key]['freq'] += value
            global_count[key]['docs'] += 1


def count_word():
    GLOBAL_COUNT = {}
    file2_content = load_content('../output/tiki_spider20141206 173643.json')
    file1_content = load_content('../output/nobita_spider20141206 133728.json')
    for each in file1_content:
        append_global_count(each['description'], GLOBAL_COUNT)
    for each in file2_content:
        append_global_count(each['description'], GLOBAL_COUNT)
    sorted_key = sorted(GLOBAL_COUNT.items(), key=lambda x: x[1], reverse=True)
    write_file('word_count.txt', {
      'number_document' : len(file1_content) + len(file2_content),
      'data' : sorted_key
    })

def write_file(namefile, content):
    json.dump(content, open(namefile,'w'), separators=(',', ': '), indent = 4)


def read_count_word():
    data = json.load(open('word_count.txt'))
    GLOBAL_COUNT = {}
    TOTAL_COUNT = data['number_document']
    for each in data['data']:
        GLOBAL_COUNT[each[0].encode('utf-8')] = each[1]['docs']
    return GLOBAL_COUNT, TOTAL_COUNT

def clean_string(string):
    string = re.sub(r'[\({\[][^\)\]}]+[\)}\]]', '', string)
    # string = string.lower()
    string = string.strip()
    return string


def mapping(string):
    it = {'I': 1, 'II': 2, 'III': 3, 'IV': 4, 'V': 5,
          'VI': 6, 'VII': 7, 'VIII': 8, 'IX': 9, 'X': 10}


def run_token(tk_wrapper):
    sentence = u"Hôm nay tôi đi học. Cuộc đời thật thú vị biết bao"
    result = tk_wrapper.token_item(sentence)
    print (result.toString()).encode('utf-8')


def tokenizer(sentences):
    global tkwrapper
    list_result = tkwrapper.token_item(sentences)
    return list_result


def test_tf_idf(listDocument):
    #load data
    data = []
    for each_file_name in listDocument:
        data = data + json.load(open(each_file_name))
    #load global count
    global_count, total_doc = read_count_word()

    print data[0]['title']
    print data[0]['description']
    for e in data[1:]:
        print
        print e['title']
        # print StringMatcher.distance(data[0]['title'].encode('utf-8'), e['title'].encode('utf-8'))
        print e['description']
        print call_tf_idf(data[0]['description'], e['description'], global_count, total_doc)
        # print

def call_tf_idf(string1, string2, global_count, total_doc):
    list_tk1 = token_count_string(string1)
    dict_tk1 = {e[0] : e[1] for e in list_tk1.items()}
    len_list_tk1 = sum(list_tk1.values())

    for (each_token, count) in list_tk1.items():
        print each_token, tf_idf(each_token, dict_tk1, len_list_tk1, global_count, total_doc)
    return

    list_tk2 = token_count_string(string2)
    dict_tk2 = {e[0] : e[1] for e in list_tk2.items()}
    len_list_tk2 = sum(list_tk2.values())

    join_list = list(set(dict_tk1.keys() + dict_tk2.keys()))

    vector_1 = [ tf_idf(each_token, dict_tk1, len_list_tk1, global_count, total_doc)[0] for each_token in join_list ]
    vector_2 = [ tf_idf(each_token, dict_tk2, len_list_tk2, global_count, total_doc)[0] for each_token in join_list ]

    return get_cosine(vector_1, vector_2)

def get_cosine(vec1, vec2):
     numerator = sum([x[0] * x[1] for x in zip(vec1, vec2)])
     sum1 = sum([x**2 for x in vec1])
     sum2 = sum([x**2 for x in vec2])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)
     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

def tf_idf(token, dict_tk, len_list_tk, global_count, total_document):
    in_string = float(dict_tk[token]) if token in dict_tk else 0
    tf = float(in_string) / float(len_list_tk)
    in_doc = float(global_count[token] + 1) if token in global_count else 1
    idf = math.log10(float(total_document)/float(in_doc))
    # print token, tf * idf, in_string, len_list_tk, tf, in_doc, total_document, idf
    return tf * idf, in_string, len_list_tk, tf, in_doc, total_document, idf


def run_token_name():
    data = load_content('../output/tiki_spider20141209 134535.json')
    for each in data:
        print '['
        for e in tokenizer(clean_string(each['description'])):
            print e
        print ']'


def matching_rate_1(string1, string2):
    len1 = len(string1)
    len2 = len(string2)
    max_len1_len2 = max(len1, len2)
    distance = StringMatcher.distance(string1, string2)
    print 'd',distance
    return float(max_len1_len2 - distance) / float(max_len1_len2)


def matching_rate_2(string1, string2):
    len1 = len(string1)
    len2 = len(string2)
    total_len1_len2 = len1 + len2
    editopt = StringMatcher.editops(string1, string2)
    total = sum([1 if x[0] != 'replace' else 2 for x in editopt])
    return float(total_len1_len2 - total) / float(total_len1_len2)



def test_string():
    tkwrapper = TokenizerWrapper()
    print sorted(tkwrapper.token_item(u'Harry Potter Và Phòng Chứa Bí Mật - Tập 2 (Tái Bản 2013)'))

if __name__ == "__main__":
    # count_word()

    # test_function1()
    # test_tf_idf(['result1', 'result2', 'result3', 'result4'])

    # test_function3()
    # test_string()

    # read_count_word()
    # file2_content = load_content('../output/tiki_spider20141206 173643.json')
    # for each in token_count_string(file2_content[0]['description']).items():
    #     print each

    # test_function2()

    # test_tf_idf(['testcase2_result1', 'testcase2_result2'])

    # globalcount , docs = read_count_word()
    # global tkwrapper
    description = u'Giới thiệu sách.    Harry Potter Và Phòng Chứa Bí Mật - Tập 2.   Harry Potter và phòng chứa bí mật , không như những bộ truyện nhiều tập khác, vẫn tuyệt hay như người anh em trước… Hogwarts là sáng tạo của một thiên tài.   - Times Literary Supplement  Harry khổ sở mong ngóng cho kì nghỉ hè kinh khủng với gia đình Dursley kết thúc. Nhưng một con gia tinh bé nhỏ tội nghiệp đã cảnh báo cho Harry biết về mối nguy hiểm chết người đang chờ cậu ở ….”  (Trích Harry Porter và Phòng Chứa Bí Mật'
    result = tkwrapper.token_item(description)
    global_count, total_doc = read_count_word()
    call_tf_idf(description, description, global_count, total_doc)

    pass
