#!/usr/bin/python
# -*- coding: utf8 -*-
__author__ = 'thinhvoxuan'
import jpype
import os




class TokenizerWrapper:

    def __init__(self):
        current_dir = os.path.dirname(os.path.realpath(__file__))
        path_file = "%s/vntokenizer.jar" % current_dir
        jpype.startJVM(jpype.get_default_jvm_path(), "-ea", "-Djava.class.path=%s"%path_file)

        property = jpype.JPackage('java.util').Properties()
        property.setProperty('sentDetectionModel', "%s/models/sentDetection/VietnameseSD.bin.gz"%current_dir)
        property.setProperty('lexiconDFA', '%s/models/tokenization/automata/dfaLexicon.xml'%current_dir)
        property.setProperty('externalLexicon', '%s/models/tokenization/automata/externalLexicon.xml'%current_dir)
        property.setProperty('normalizationRules', '%s/models/tokenization/normalization/rules.txt'%current_dir)
        property.setProperty('lexers', '%s/models/tokenization/lexers/lexers.xml'%current_dir)
        property.setProperty('unigramModel', '%s/models/tokenization/bigram/unigram.xml'%current_dir)
        property.setProperty('bigramModel', '%s/models/tokenization/bigram/bigram.xml'%current_dir)
        property.setProperty('namedEntityPrefix', '%s/models/tokenization/prefix/namedEntityPrefix.xml'%current_dir)

        # token_file = "%s/tokenizer.properties" % os.path.dirname(os.path.realpath(__file__))
        self.VietTokenizer_class = jpype.JPackage('vn.hus.nlp.tokenizer').VietTokenizer
        self.VietTokenizer = self.VietTokenizer_class(property)
        self.StringrReader_class = jpype.JPackage('java.io').StringReader

    def token_item(self, string_input):
        import re
        string_input = re.sub(r'[\({\[][^\)\]}]+[\)}\]]', '', string_input)
        token = self.VietTokenizer_class.getTokenizer()
        token.tokenize(self.StringrReader_class(string_input))
        result = token.getResult()

        return to_utf_8(result)

    def token_sample(self, string_input):
        output = self.VietTokenizer.tokenize(string_input)
        return to_utf_8(output)

    def __del__(self):
        jpype.shutdownJVM()


def to_utf_8(string_or_list):
    if not isinstance(string_or_list, basestring):
        ls = [truncate_string((item.toString())).encode('utf-8') for item in string_or_list]
        return [item for item in ls if len(item) > 0]
    else:
        return (string_or_list.toString()).encode('utf-8')

def truncate_string(string):
    import re
    return re.sub('[-\(\)\.{}&]', '', string)

if __name__ == "__main__":
    tkwrapper = TokenizerWrapper()
    sentence = u"Hôm nay tôi đi học. Cuộc đời thật thú vị biết bao"
    result = tkwrapper.token_item(sentence)
    for each in result:
        print (each.toString()).encode('utf-8')
    print (result.toString()).encode('utf-8')
    pass