from mobileCrawler.util import function_helper

__author__ = 'thinhvoxuan'

import scrapy

from mobileCrawler.util.value import *


def create_item_class(class_field, field_dict_input, ls_pip_line = []):
    """
    >>> class_fields = create_property('tiki')
    >>> create_item_class(class_fields, {})
    <class 'scrapy.item.Tiki'>
    """

    field_dict = {}
    for field_name in field_dict_input:
        field_dict[field_name] = scrapy.Field()
    field_dict[item_fields] = field_dict_input

    default_dict_input = all_default_field()
    for field_name in default_dict_input:
        field_dict[field_name] = scrapy.Field()
    field_dict[item_fields_default] = default_dict_input

    field_dict[POST_PROCESS_METHOD] = post_process
    field_dict[LIST_PIPLINE_FIELD] = ls_pip_line

    return type(class_field[CLASS_NAME], (scrapy.Item, ), field_dict)


def all_default_field():
    return {
        item_fields_default_link: create_field_dict(
            STRING_TYPE
        ),
        item_fields_default_parent_link : create_field_dict(
            STRING_TYPE
        ),
        item_fields_default_source: create_field_dict(
            STRING_TYPE
        ),
        item_fields_default_spider_name: create_field_dict(
            STRING_TYPE
        ),
        item_fields_default_update_date: create_field_dict(
            STRING_TYPE
        ),
    }

def create_field_dict(type_field, href='',  pre_method=[], post_method=[]):
    """ create field dictionary
    >>> result = create_field_dict(STRING_TYPE, '//title')
    >>> {   'HREF_FIELD' : '//title',\
            'PRE_METHOD_FIELD' : [],\
            'POST_METHOD_FIELD': [],\
            'DEFAULT_VALUE_FIELD' : '',\
            'TYPE_FIELD': 'STRING_TYPE'\
        } == result
    True
    """
    if type_field == STRING_TYPE:
        default_value = ""
    elif type_field == NUMBER_TYPE:
        default_value = 0
    else:
        default_value = ""

    return {
        HREF_FIELD : href,
        PRE_METHOD_FIELD : pre_method,
        POST_METHOD_FIELD: post_method,
        DEFAULT_VALUE_FIELD : default_value,
        TYPE_FIELD: type_field
    }


def create_property(class_name, tracking_time=True, remove_params_link=True ):
    """
    >>> result = create_property('tiki')
    >>> result[CLASS_NAME] == 'Tiki'
    True
    """
    return {
        CLASS_NAME : class_name.title(),
        TRACKING_TIME : tracking_time,
        REMOVE_PARAMS_LINK :remove_params_link
    }


def post_process(item):
    for name, value in item.item_fields.items():
        for method in value[POST_METHOD_FIELD]:
            item[name] = getattr(function_helper, method)(item[name])
    return item
