__author__ = 'thinhvoxuan'

from abstract_item import create_field_dict, create_item_class, create_property
from mobileCrawler.util.value import *
from mobileCrawler.util.function_helper import *

def get_nobita_class():

    item_fields = {
        'title': create_field_dict(
            STRING_TYPE,
            '//div[@class="product_info"]/h1/text()',
            post_method=[remove_all_tag_func]
        ),
        'old_price': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="vrootprice"]/span/text()',
            post_method=[get_only_number_func]
        ),
        'reg_price': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="saleprice"]/span/text()',
            post_method=[get_only_number_func]
        ),
        'sale_off': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="saleprice"]/span/text()',
            post_method=[get_only_number_func]
        ),
        'description': create_field_dict(
            STRING_TYPE,
            '//div[@id="module_ProductDetail"]',
            post_method=[remove_all_tag_func]
        ),
        'information': create_field_dict(
            DICT_TYPE,
            '//tr[contains(@class, "field_view_contenner")]//td//text()',
            post_method=[zip_information_func, remove_all_tag_func]
        ),
        'reference': create_field_dict(
            STRING_TYPE,
            '//div[@id="module_AlsoBought"]//div[@class="productname"]//a/@href',
        ),
        'author': create_field_dict(
            STRING_TYPE,
            '//div[@class="viewfields"]/span[1]/a/text()',
        ),
        'book_image': create_field_dict(
            STRING_TYPE,
            '(//div[@class="mainimage"]//img[1]/@src)[1]',
        ),
    }
    class_fields = create_property('nobita')
    return create_item_class(class_fields, item_fields,
                             ls_pip_line=['WriteToJSONPipeline', 'WriteToJSON_RA_Pipeline', 'AdjustPrice'])