__author__ = 'thinhvoxuan'

from abstract_item import create_field_dict, create_item_class, create_property
from mobileCrawler.util.value import *
from mobileCrawler.util.function_helper import *

def get_vinabook_class():

    item_fields = {
        'title': create_field_dict(
            STRING_TYPE,
            '//h1[@class="mainbox-title"]/text()',
            post_method=[remove_all_tag_func]
        ),
        'old_price': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="product-main-info"]//span[@class="list-price-label"]/following::span[contains(@class,"list-price")][1]//text()',
            post_method=[get_only_number_func]
        ),
        'reg_price': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="product-main-info"]//span[@class="price-num"][1]//text()',
            post_method=[get_only_number_func]
        ),
        'sale_off': create_field_dict(
            NUMBER_TYPE,
            '//div[@class="product-main-info"]//span[@class="price-num"][1]//text()',
            post_method=[get_only_number_func]
        ),
        'description': create_field_dict(
            STRING_TYPE,
            '//div[@class="full-description"]//text()',
            post_method=[remove_all_tag_func]
        ),
        'information': create_field_dict(
            DICT_TYPE,
            '//div[@class="product-feature"]/ul/li//text()',
            post_method=[zip_information_func, remove_all_tag_func]
        ),
        'reference': create_field_dict(
            STRING_TYPE,
            '//div[@class="mainbox2-body"]//a[@class="product-title"]/@href',
        ),
        'author': create_field_dict(
            STRING_TYPE,
            '//div[@class="viewfields"]/span[1]/a/text()',
        ),
    }
    class_fields = create_property('vinabook')
    return create_item_class(class_fields, item_fields,
                             ls_pip_line=['WriteToJSONPipeline', 'WriteToJSON_RA_Pipeline', 'AdjustPrice'])