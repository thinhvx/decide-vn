__author__ = 'thinhvoxuan'


from abstract_item import create_field_dict, create_item_class, create_property
from mobileCrawler.util.value import *
from mobileCrawler.util.function_helper import *

def get_tiki_class():
    """
    >>> CLASS_TIKI = get_tiki_class()
    >>> tk = CLASS_TIKI()
    >>> tk['title'] = 'this is title'
    >>> tk['title'] == 'this is title'
    True
    >>> 'title' in tk.keys()
    True
    """
    item_fields = {
        'title': create_field_dict(
            STRING_TYPE,
            '//*[@id="product_addtocart_form"]/div/div[3]/h1/text()',
            post_method=[remove_all_tag_func]
        ),
        'old_price': create_field_dict(
            NUMBER_TYPE,
            '//p[@class="old-price"]/span[@class="price"]/text()',
            post_method=[get_only_number_func]
        ),
        'reg_price': create_field_dict(
            NUMBER_TYPE,
            '//span[@class="regular-price"]/span/text()',
            post_method=[get_only_number_func]
        ),
        'sale_off': create_field_dict(
            NUMBER_TYPE,
            '//p[@class="special-price2"]/span/span[@class="price"]/text()',
            post_method=[get_only_number_func]
        ),
        'description': create_field_dict(
            STRING_TYPE,
            '//div[@class="product-description"]',
            post_method=[remove_all_tag_func]
        ),
        'information': create_field_dict(
            DICT_TYPE,
            "//div[@id='chi-tiet']//tr/td",
            post_method=[zip_information_func, remove_all_tag_func]
        ),
        'reference': create_field_dict(
            STRING_TYPE,
            '//div[contains(@class, "b-product-silder__content")]//a[contains(@class,"b-product-silder__item-link")]/@href',
            post_method=[create_dict_func]
        ),
        'author': create_field_dict(
            STRING_TYPE,
                '//div[contains(@class,"brand-box")]//ul//b//text()',
        ),
        'book_image': create_field_dict(
            STRING_TYPE,
            '(//div[contains(@class, "item-main-image")]//img[1]/@src)[1]',
        ),
    }
    class_fields = create_property('tiki')
    return create_item_class(class_fields, item_fields,
                             ls_pip_line=['WriteToJSONPipeline', 'AdjustPrice', 'WriteToJSON_RA_Pipeline'])