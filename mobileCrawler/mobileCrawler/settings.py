# -*- coding: utf-8 -*-

# Scrapy settings for mobileCrawler project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

import mobileCrawler.pipline.adjustpricepipline
from mobileCrawler.pipline.json_exporter_pipline import WriteToJSONPipeline
BOT_NAME = 'mobileCrawler'

SPIDER_MODULES = ['mobileCrawler.spiders']
NEWSPIDER_MODULE = 'mobileCrawler.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.93 Safari/537.36"
DEPTH_LIMIT = 0



DOWNLOADER_MIDDLEWARES = {
    # 'mobileCrawler.middleware.middleware.CustomHttpProxyMiddleware': 543,
    # 'mobileCrawler.middleware.middleware.CustomUserAgentMiddleware': 545,
}

ITEM_PIPELINES = {
    'mobileCrawler.pipline.adjustpricepipline.AdjustPrice': 200,
    'mobileCrawler.pipline.json_exporter_pipline.WriteToJSONPipeline': 300,
    'mobileCrawler.pipline.json_ra_exporter_pipline.WriteToJSON_RA_Pipeline': 301
}

SPIDER_CONTRACTS = {
    'tests.test_spiders.test_tiki_spider.Test_Parse_TikiSpider' : 50,
    'tests.test_spiders.test_tiki_spider.Test_Parse_Item_TikiSpider' : 51
}


RANDOMIZE_DOWNLOAD_DELAY = True
# DOWNLOAD_DELAY = 2 #delay 2 second

# CONCURRENT_REQUESTS = 100
# CONCURRENT_REQUESTS_PER_IP = 8
# CONCURRENT_REQUESTS = 1
# CONCURRENT_REQUESTS_PER_DOMAIN = 1
# CONCURRENT_ITEMS = 1

# LOG_ENABLED = False
# Log_FILE = '/vagrant/log/scrapy.log'
# LOG_STDOUT = True
