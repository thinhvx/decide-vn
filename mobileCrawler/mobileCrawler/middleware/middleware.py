from scrapy import log
from proxy import PROXIES
from agents import AGENTS

import random

"""
Custom proxy provider. 
"""
class CustomHttpProxyMiddleware(object):
    
    def process_request(self, request, spider):
        # TODO implement complex proxy providing algorithm
        if self.use_proxy(request):
            p = random.choice(PROXIES)
            try:
                log.msg("Choose proxy %s" % p['ip_port'])
                request.meta['proxy'] = 'http://127.0.0.1:8123'
            except Exception, e:
                log.msg("Exception %s" % e, _level=log.CRITICAL)
                
    
    def use_proxy(self, request):
        """
        using direct download for depth <= 2
        using proxy with probability 0.3
        """

        # if "depth" in request.meta and int(request.meta['depth']) <= 2:
        #     return False
        # i = random.randint(1, 10)
        # return True
        return True
    
    
"""
change request header nealy every time
"""
class CustomUserAgentMiddleware(object):
    def process_request(self, request, spider):
        agent = random.choice(AGENTS)
        request.headers['User-Agent'] = agent
