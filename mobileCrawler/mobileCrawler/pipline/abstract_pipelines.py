# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from mobileCrawler.util.value import LIST_PIPLINE_FIELD

# class MobilecrawlerPipeline(object):
#     def process_item(self, item, spider):
#         return item

class AbstractPipline(object):
    def __init__(self):
        pass

    def process_item(self, item, spider):
        if type(self).__name__ not in getattr(item, LIST_PIPLINE_FIELD):
            return item
        return self.pip_item(item, spider)

    def pip_item(self, item, spider):
        return item

    def open_spider(self, spider):
        pass

    def close_spider(self, spider):
        pass


    def getClassName(self):
        pass