__author__ = 'thinhvoxuan'
from abstract_pipelines import AbstractPipline
from datetime import datetime
import json
from mobileCrawler.util.value import *
from mobileCrawler.util.function_helper import remove_accent, remove_accent_dict

class WriteToJSON_RA_Pipeline(AbstractPipline):
    def __init__(self):
        self.file = None

    def pip_item(self, item, spider):

        for name, value in item.item_fields.items():
            if value[TYPE_FIELD] == STRING_TYPE:
                item[name] = remove_accent(item[name])
            if value[TYPE_FIELD] == DICT_TYPE:
                item[name] = remove_accent_dict(item[name])

        line = json.dumps(dict(item))

        if not self.file:
            today = datetime.now()
            fileName = 'output/' + spider.name + today.strftime("%Y%m%d %H%M%S") + '_ra.json'
            self.file = open(fileName, 'wb')
            self.file.write("[")
            self.file.write(line)
        else:
            line = ",\n" + line
            self.file.write(line)
        return item

    def close_spider(self, spider):
        if self.file:
            self.file.write("]")
            self.file.close()