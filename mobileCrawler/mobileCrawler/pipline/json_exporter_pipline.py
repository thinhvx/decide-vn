__author__ = 'thinhvoxuan'
from abstract_pipelines import AbstractPipline
from datetime import datetime
import json


class WriteToJSONPipeline(AbstractPipline):
    def __init__(self):
        self.file = None

    def pip_item(self, item, spider):
        line = json.dumps(dict(item))
        if not self.file:
            today = datetime.now()
            filename = 'output/' + spider.name + today.strftime("%Y%m%d %H%M%S") + '.json'
            self.file = open(filename, 'wb')
            self.file.write("[")
            self.file.write(line)
        else:
            line = ",\n" + line
            self.file.write(line)
        return item

    def close_spider(self, spider):
        if self.file:
            self.file.write("]")
            self.file.close()