__author__ = 'thinhvoxuan'
from abstract_pipelines import AbstractPipline


class AdjustPrice(AbstractPipline):
    def pip_item(self, item, spider):
        if item['old_price'] == 0:
            item['old_price'] = item['reg_price'] + item['old_price']
            item['reg_price'] = item['old_price']
        else:
            item['reg_price'] = item['sale_off']
        return item