# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from BeautifulSoup import BeautifulSoup
import re


class MobilecrawlerItem(scrapy.Item):
    link = scrapy.Field()
    title = scrapy.Field()
    title_encode = scrapy.Field()
    name = scrapy.Field()
    description = scrapy.Field()


class BookCrawlerItem(scrapy.Item):
    link = scrapy.Field()
    title = scrapy.Field()
    old_price = scrapy.Field()
    reg_price =scrapy.Field()
    sale_off = scrapy.Field()
    description = scrapy.Field()
    information = scrapy.Field()
    reference = scrapy.Field()
    author = scrapy.Field()


    item_fields = {
        'title' :       '//*[@id="product_addtocart_form"]/div/div[3]/h1/text()',
        'old_price' :   '//p[@class="old-price"]/span[@class="price"]/text()',
        'reg_price':    '//span[@class="regular-price"]/span/text()',
        'sale_off':     '//p[@class="special-price2"]/span/span[@class="price"]/text()',
        'description':  '//div[@class="product-description"]',
        'information':  '//*[@id="chi-tiet"]',
        'reference' :   '//div[contains(@class, "b-product-silder__content")]',
        'author' :      '//ul[@class="brand-list"]/li[1]/a/b',
    }

    def postProcessText(self, text):
        return text
