__author__ = 'thinhvoxuan'
from abstract_spider import AbstractSpider
from mobileCrawler.item.book_nobita import get_nobita_class

class NobitaSpider(AbstractSpider):
    name = "nobita_spider"
    allowed_domains = ["nobita.vn"]

    start_urls = (
        'http://nobita.vn/danh-muc/1/sach-kinh-te.html',
        'http://nobita.vn/danh-muc/2/van-hoc-nuoc-ngoai.html',
        'http://nobita.vn/danh-muc/3/van-hoc-trong-nuoc.html',
        'http://nobita.vn/danh-muc/4/sach-ki-nang-song.html',
        'http://nobita.vn/danh-muc/5/sach-tuoi-teen.html',
        'http://nobita.vn/danh-muc/6/hoc-ngoai-ngu.html',
        'http://nobita.vn/danh-muc/7/sach-thieu-nhi.html',
        'http://nobita.vn/danh-muc/8/thuong-thuc-doi-song.html',
        'http://nobita.vn/danh-muc/9/sach-chuyen-nganh.html',
        'http://nobita.vn/danh-muc/51/sach-van-hoa-nghe-thuat.html'
    )

    xpath_list = '//div[@class="productname"]/a/@href'
    xpath_nextPage = '//ul[@class="pagenav"]//li[@class="active"]/following::a[1]/@href'
    maxDeep = 50

    def get_item_class(self):
        return get_nobita_class()
