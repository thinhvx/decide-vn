__author__ = 'thinhvoxuan'
from abstract_spider import AbstractSpider
from mobileCrawler.item.book_tiki import get_tiki_class

class TikiSpider(AbstractSpider):
    name = "tiki_spider"
    allowed_domains = ["tiki.vn", "tikicdn.com"]

    start_urls = (
        'http://tiki.vn/sach-truyen-tieng-viet/sach-van-hoc.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-kinh-te.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-chuyen-nganh.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-ky-nang-song-dep.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-giao-khoa.html',
        'http://tiki.vn/sach-truyen-tieng-viet/ngoai-ngu-tu-dien.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-teen.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-truyen-thieu-nhi.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-thuong-thuc-doi-song.html',
        'http://tiki.vn/sach-truyen-tieng-viet/truyen-tranh.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-van-hoa-nghe-thuat-du-lich.html',
        'http://tiki.vn/sach-truyen-tieng-viet/tap-chi.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-nuoi-day-con.html',
        'http://tiki.vn/sach-truyen-tieng-viet/sach-van-hoc-phuong-tay.html',

        'http://tiki.vn/sach-tieng-anh/new-york-times-best-sellers.html',
        'http://tiki.vn/sach-tieng-anh/business-investing.html',
        'http://tiki.vn/sach-tieng-anh/children-books.html',
        'http://tiki.vn/sach-tieng-anh/fiction-literature.html',
        'http://tiki.vn/sach-tieng-anh/foreign-language-learning.html',
        'http://tiki.vn/sach-tieng-anh/how-to-advice.html',
        'http://tiki.vn/sach-tieng-anh/memoirs-biographies.html',
        'http://tiki.vn/sach-tieng-anh/social-sciences.html',
        'http://tiki.vn/sach-tieng-anh/teens.html',
        'http://tiki.vn/sach-tieng-anh/awards.html'
        'http://tiki.vn/sach-tieng-anh/culture-and-art.html',
    )

    xpath_list = "//a[@class='b-product-item__link']/@href"
    xpath_nextPage = "//li[contains(@class, 'b-pager__paging-arrow_next')]/a/@href"
    maxDeep = 100

    def parse(self, response):
        """
        @url http://tiki.vn/new-products/sach-truyen-tieng-viet.html
        @test_parse_tiki_spider
        """
        return super(TikiSpider, self).parse(response)

    def parse_item(self, response):
        """
        @url http://tiki.vn/nguyen-nhat-anh-va-toi-p114510.html?ref=c316.c531.c714.
        @test_parse_item_tiki_spider
        """
        return super(TikiSpider, self).parse_item(response)

    def get_item_class(self):
        return get_tiki_class()
