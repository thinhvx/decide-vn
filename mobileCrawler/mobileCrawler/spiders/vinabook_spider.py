__author__ = 'thinhvoxuan'
from abstract_spider import AbstractSpider
from mobileCrawler.item.book_nobita import get_nobita_class

class VinabookSpider(AbstractSpider):
    name = "vinabook_spider"
    allowed_domains = ["www.vinabook.com/"]

    start_urls = (
        'http://www.vinabook.com/c348/kinh-te/page-1/',
    )

    xpath_list = '//a[@class="product-title"]/@href'
    xpath_nextPage = '//span[@class="pagination-selected-page"]/following::a[1]/@href'
    maxDeep = 30

    def get_item_class(self):
        return get_nobita_class()
