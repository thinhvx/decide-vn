__author__ = 'thinhvoxuan'
# -*- coding: utf-8 -*-
# from scrapy import
from scrapy.contrib.spiders import CrawlSpider
from scrapy.selector import Selector
from scrapy.http import Request
import urlparse
from datetime import date
from scrapy.cmdline import execute

from mobileCrawler.util.value import *


class AbstractSpider(CrawlSpider):
    name = ""
    allowed_domains = [""]
    start_urls = (
        "",
    )
    xpath_list = ""
    xpath_nextPage = ""
    pipelines = []
    maxDeep = 3

    def appendDomains(self, link):
        if not link.count(self.allowed_domains[0]):
            link = "%s/%s"%(self.allowed_domains[0], link)
            link = link.replace('//', '/')
        if not link.count('http://'):
            return "http://%s" %(link)
        else:
            return link

    def parse(self, response):
        current_url = response.url
        sel = Selector(response)
        list_link = sel.xpath(self.xpath_list).extract()
        if not list_link:
            print 'nothing'
        else:
            for each_link in list_link:
                yield Request(url= self.appendDomains(each_link), callback=self.parse_item, meta={'parent_url': current_url})

        if 'deep' in  response.request.meta:
            currentDeep = response.request.meta['deep'] + 1
        else:
            currentDeep = 1

        if currentDeep <= self.maxDeep:
            next_page = self.get_NextLink(response)
            if next_page:
                next_page = next_page[0].extract()
                # print 'crawl: ', next_page
                yield Request(url=self.appendDomains(next_page), callback=self.parse, meta={'deep' : currentDeep, 'parent_url': current_url})

    def get_NextLink(self, response):
        return response.xpath(self.xpath_nextPage)

    def parse_item(self, response):
        item = self.get_item_instance()

        item[item_fields_default_link] = response.url
        if 'parent_url' in response.request.meta:
            item[item_fields_default_parent_link] = response.request.meta['parent_url']
        item[item_fields_default_spider_name] = self.name
        item[item_fields_default_source] = self.allowed_domains[0]
        item[item_fields_default_update_date] = date.today().strftime("%Y-%m-%d")

        for name, value in item.item_fields.items():
            if value[HREF_FIELD]:
                item[name] = " ;".join(response.xpath(value[HREF_FIELD]).extract())
                # if not item[name] or item[name] == '':
                #     print value[DEFAULT_VALUE_FIELD]
                #     item[name] = value[DEFAULT_VALUE_FIELD]
        # print item['reg_price']
        item['book_image'] = self.appendDomains(item['book_image'])
        item = item.POST_PROCESS_METHOD()
        return item

    def get_item_class(self):
        """
            RETURN ITEM CLASS
        """
        return None

    def get_item_instance(self):
        CLASS = self.get_item_class()
        if not CLASS:
            raise Exception('missing class ITEM')
        return CLASS()