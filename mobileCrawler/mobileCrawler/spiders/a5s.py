# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.spiders import Rule, CrawlSpider
from scrapy.contrib.linkextractors import LinkExtractor
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import Selector


class A5sSpider(scrapy.Spider):
    name = "5s"
    allowed_domains = ["https://www.5giay.vn/"]
    start_urls = (
        'https://www.5giay.vn/mobile.html',
    )
    rules = [Rule(LinkExtractor(allow_domains='https://www.5giay.vn/'), 'parse_items')]

    def parse(self, response):
        title = response.xpath('//title/text()').extract()
        print title
