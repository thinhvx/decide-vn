 #!/usr/bin/python
 # -*- coding: utf-8 -*-

__author__ = 'thinhvoxuan'
import re
from BeautifulSoup import BeautifulSoup
from unidecode import unidecode
import json

join_multi_string_func = 'join_multi_string'


def join_multi_string(list_string):
    """
    :type list_string dict
    >>> result = join_multi_string(['a', 'B', 'c'])
    >>> "a B c" == result
    True
    """
    print list_string
    return " ".join(list_string)

get_only_number_func = 'get_only_number'


def get_only_number(input_value):
    """
    >>> get_only_number("$1.2,5.5--")
    1255
    """
    if type(input_value) is int:
        return input
    input_value = re.sub(r'[^0-9]', '', input_value)
    if len(input_value) == 0:
        return 0
    else:
        return int(input_value)

remove_all_tag_func = 'remove_all_tag'


def remove_all_tag(htmlString):
    """
    :type htmlString str
    >>> result = remove_all_tag("<tag><open href='something'/> content  </tag>")
    >>> result == " content "
    True
    """
    textHtml = BeautifulSoup(htmlString)
    return textHtml.getText(separator=u' ')

remove_param_in_url_func = 'remove_param_in_url'


def remove_param_in_url(url):
    """
    :type url: str
    >>> remove_param_in_url("http://tiki.vn/nguyen-cong-hoan-tuyen-tap-p48065.html?ref=c845.c2288.c2495.&src=upsell")
    'http://tiki.vn/nguyen-cong-hoan-tuyen-tap-p48065.html'
    """
    idxQ = url.find("?")
    return url[:idxQ]

remove_accent_func = 'remove_accent'


def remove_accent(string):
    """
    >>> remove_accent(u'nguy\u1ec5n c\xf4ng hoan')
    'nguyen cong hoan'
    """
    # string = unicode(string, encoding='UTF-8')
    return unidecode(string)


def remove_accent_dict(string):
    data = dict(json.loads(string))
    output = {}
    for key, value in data.items():
        output[unidecode(key)] = unidecode(value)
    return json.dumps(output)

zip_information_func = 'zip_information'

def zip_information(string):
    listInformation = string.split(" ;")
    listInformation = [e.strip() for e in listInformation if len(e.strip()) > 0]

    dict_output = {}
    for key, value in zip(listInformation[0::2], listInformation[1::2]):
        dict_output[key] = value
    return json.dumps(dict_output, ensure_ascii = False)


create_dict_func = 'create_dict'
def create_dict(string):
    string = string.strip()
    listReferences = string.split(" ;")
    return json.dumps(listReferences, ensure_ascii=False)