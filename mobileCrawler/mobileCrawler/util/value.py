__author__ = 'thinhvoxuan'


HREF_FIELD = 'HREF_FIELD'
PRE_METHOD_FIELD = 'PRE_METHOD_FIELD'
POST_METHOD_FIELD = 'POST_METHOD_FIELD'
DEFAULT_VALUE_FIELD = 'DEFAULT_VALUE_FIELD'
TYPE_FIELD = 'TYPE_FIELD'
LIST_PIPLINE_FIELD = "LIST_PIPLINE_FIELD"

NUMBER_TYPE = 'NUMBER_TYPE'
STRING_TYPE = 'STRING_TYPE'
DICT_TYPE = 'DICT_TYPE'


CLASS_NAME = 'CLASS_NAME'
TRACKING_TIME = 'TRACKING_TIME'
TRACKING_TIME_FIELD = 'TIME_FIELD'
REMOVE_PARAMS_LINK = 'REMOVE_PARAMS_LINK'


POST_PROCESS_METHOD = 'POST_PROCESS_METHOD'


item_fields = 'item_fields'

item_fields_default = 'item_fields_default'
item_fields_default_link = 'link'
item_fields_default_source = 'source'
item_fields_default_update_date = 'update_date'
item_fields_default_spider_name = 'spider_name'
item_fields_default_parent_link = 'parent_link'
