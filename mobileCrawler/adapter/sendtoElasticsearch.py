__author__ = 'thinhvoxuan'
import json
import rson
from elasticsearch import Elasticsearch

# uploadFile = 'output/2014-10-03 08:50:42.037168.json'
# uploadFile = '../output/2014-10-06 12:02:59.365866.json'
# uploadFile = '../output/2014-10-09 04:52:23.672842.json'
uploadFile = 'output_merge'

# import os
# for fileName in os.listdir('output'):
#     print fileName

uploadlist_file = [
    'tiki_spider20141203 075845.json',
    'tiki_spider20141206 173643.json',
    'tiki_spider20141209 144740.json',
    'tiki_spider20141215 042841.json',
]
for uploadFile in uploadlist_file:
    with open('../output/' + uploadFile) as json_data:
        data = rson.loads(json_data.read())
        es = Elasticsearch(hosts = 'backend.thinhvoxuan.me')
        for each in data:
            each['update_date'] = each['update_date'].replace('/', '-')
            # print each['update_date']
            res  = es.index(index = 'book', doc_type = 'tiki', body=each)
            print res['created']