__author__ = 'thinhvoxuan'
import json
import rson
from elasticsearch import Elasticsearch

# uploadFile = 'output/2014-10-03 08:50:42.037168.json'
# uploadFile = '../output/2014-10-06 12:02:59.365866.json'
# uploadFile = '../output/2014-10-09 04:52:23.672842.json'
# uploadFile = '../output/nobita_spider20141228 013556.json'

# import os
# for fileName in os.listdir('output'):
#     print fileName

uploadlist_file = [
    # 'nobita_spider20141203 072125.json',
    # 'nobita_spider20141204 194435.json',
    # 'nobita_spider20141205 132605.json',
    # 'nobita_spider20141206 133728.json',
]
for uploadFile in uploadlist_file:
    with open('../output/' + uploadFile) as json_data:
        data = rson.loads(json_data.read())
        es = Elasticsearch(hosts = 'backend.thinhvoxuan.me')
        for each in data:
            each['update_date'] = each['update_date'].replace('/', '-')
            # print each['update_date']
            res  = es.index(index = 'book', doc_type = 'nobita', body=each)
            print res['created']
