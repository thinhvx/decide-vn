__author__ = 'thinhvoxuan'
from elasticsearch import Elasticsearch
import rson

SERVER_NAME = 'backend.thinhvoxuan.me'

class ElasticsearchWrapper:
    SERVER_NAME = 'backend.thinhvoxuan.me'

    def __init__(self):
        self.es = Elasticsearch(hosts=[self.SERVER_NAME])

    def check_ping(self):
        return self.es.ping()

    def create_or_recreate_index(self, index_name):
        self.es.indices.delete(index=index_name, ignore=[400, 404])
        return self.es.indices.create(index_name)

    def close_index(self, index_name):
        self.es.indices.close(index_name)

    def open_index(self, index_name):
        self.es.indices.open(index_name)


    def create_or_recreate_doctypes(self, index_name, doc_type, body):
        return self.es.indices.put_mapping(index=index_name, doc_type=doc_type, body=body)

    def get_doctype(self, index_name, doc_type):
        return self.es.indices.get_mapping(index=index_name, doc_type=doc_type)

    def create_analyzer(self, index_name):
        settings = {
            "settings":{
               "analysis": {
                    "analyzer": {
                        "folding": {
                            "tokenizer": "standard",
                                "filter":  [ "lowercase", "asciifolding" ]
                        }
                    }
                }
            }
        }
        return self.es.indices.put_settings(settings, index=index_name)

    def index_data(self, index, doc_type, file_name):
        json_data = open(file_name)
        data = rson.loads(json_data.read())
        for each in data:
            # t = rson.loads(each['information'])
            # each['information'] = t
            res = self.es.index(index=index, doc_type= doc_type, body= each)
            print res['created']



    def __del__(self):
        pass

def get_tiki():
    tiki = {
        'tiki': {
            'properties': {
                "parent_link": {
                    "type": "string"
                },
                "spider_name": {
                    "type": "string"
                },
                "sale_off": {
                    "type": "long"
                },
                "description": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "reg_price": {
                    "type": "long"
                },
                "update_date": {
                    "type": "date"
                },
                "title": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "old_price": {
                    "type": "long"
                },
                "source": {
                    "type": "string"
                },
                "link": {
                    "type": "string"
                },
                "author": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "information": {
                    "type": "string"
                },
                "reference": {
                    "type": "string"
                },
                "book_image":{
                    "type": "string"
                }
            }
        }
    }
    return tiki

def get_nobita():
    nobita = {
        'nobita': {
            'properties': {
                "parent_link": {
                    "type": "string"
                },
                "spider_name": {
                    "type": "string"
                },
                "sale_off": {
                    "type": "long"
                },
                "description": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "reg_price": {
                    "type": "long"
                },
                "update_date": {
                    "type": "date"
                },
                "title": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "old_price": {
                    "type": "long"
                },
                "source": {
                    "type": "string"
                },
                "link": {
                    "type": "string"
                },
                "author": {
                    "type": "string",
                    "analyzer": "standard",
                    "fields": {
                        "folded": {
                            "type": "string",
                            "analyzer": "folding"
                        }
                    }
                },
                "information": {
                    "type": "string"
                },
                "reference": {
                    "type": "string"
                },
                "book_image":{
                    "type": "string"
                }
            }
        }
    }
    return nobita

def create_index():
    import time
    ids = ElasticsearchWrapper()
    print ids.check_ping()
    time.sleep(1)
    print ids.create_or_recreate_index('book')
    time.sleep(1)
    print ids.close_index('book')
    time.sleep(1)
    print ids.create_analyzer('book')
    time.sleep(1)
    print ids.open_index('book')
    time.sleep(1)
    print ids.create_or_recreate_doctypes(index_name='book', doc_type='tiki', body=get_tiki())
    time.sleep(1)
    print ids.get_doctype(index_name='book', doc_type='tiki')

def create_nobita_index():
    ids = ElasticsearchWrapper()
    print ids.create_or_recreate_doctypes(index_name='book', doc_type='nobita', body=get_nobita())
    print ids.get_doctype(index_name='book', doc_type='nobita')

if __name__ == "__main__":
    create_index()
    create_nobita_index()


