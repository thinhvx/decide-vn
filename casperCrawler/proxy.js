var casper = require('casper').create();
casper.options.waitTimeout = 1000;

var link_proxy = "80";
var port = 80;

DEBUG = true;

var echo = function (str){
    if (DEBUG)
        casper.echo(str);
}

var writeFileBegin = function(){
    echo('PROXIES = [');
}

var writeFileEnd = function(){
    echo("]")
}

var writeFile = function(results, port){
    for(var i=0; i<results.length; i++){
        var ip = results[i];
        var string = '    {\"ip_port\": "' + ip + ':' + port +'\"},';
        echo(string)
    }
}

function getLinks() {
    var links = document.querySelectorAll('tr.proxy td:nth-child(2)');
    var result = [];
    for (i = 0; i < links.length; i++)
        result[i] = links[i].innerText;
    return result;
}

var all_ports = [
    80,
    8080,
    3128
];

casper.start();

casper.then(function () {
    writeFileBegin();
});

casper.each(all_ports, function(self, port) {
    link = 'http://www.gatherproxy.com/proxylist/port/' + port;
    self.thenOpen(link, function() {
        var all_ip = this.evaluate(getLinks);
        writeFile(all_ip, port);
    });
});

casper.then(function () {
    writeFileEnd();
})

casper.wait(1000);
casper.run();
