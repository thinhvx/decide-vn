/**
 * Created by thinhvoxuan on 10/16/14.
 */
//https://github.com/yotsumoto/casperjs-goto/blob/master/infiniteloop.js
var casper = require('casper').create();
var utils = require('utils');
var f = utils.format;

require('./casperloop');


var output_global = {}

function getLinks() {
    var links = document.querySelectorAll('td.fh');
    var links2 = document.querySelectorAll('td.fs');
    var links3 = document.querySelectorAll('td.fa');
    var output_local = {}
    for(var i=0; i < links.length; i++){
        var e = links[i].innerText + ' ' + links2[i].innerText + ' ' + links3[i].innerText;
        output_local[links[i].innerText] = e;
    }
    return output_local;
}

casper.start();
casper.label('BEGIN');

casper.thenOpen('http://www.livescore.com/soccer/live/', function() {
    var me = this;
    var output = this.evaluate(getLinks);
    for (var key in output){
        if (!output_global.hasOwnProperty(key) || output_global[key] != output[key]){
            me.echo('Update: ' + output[key]);
            output_global[key] = output[key];
            me.echo(' ');
        }
    }
    for(var key in output_global){
        if (!output.hasOwnProperty(key)){
            me.echo('');
            me.echo('Game stop: ');
            me.echo(output_global[key]);
            me.echo('');
            delete output_global[key];

        }
    }
});

casper.wait(5000, function(){
     this.goto('BEGIN');
});

casper.run();
