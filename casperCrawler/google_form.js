var casper = require('casper').create();
casper.options.waitTimeout = 1000;

var link_google = "https://docs.google.com/forms/d/1Uklogy8vvILzb6iPwugby8Yn_N-cA1HCyxUDllKaNfc/viewform";

DEBUG = true;

var gen_input_with_value = function(value){
    return 'input[value="' + value + '"]';
};

var gen_input_with_name_value = function(name, value){
    return 'input[value="' + value + '"][name="' + name + '"]';
};

var random_from_list = function(list){
    var index = Math.floor((Math.random() * list.length));
    return list[index];
};

var helper_check_with_name_value = function(name, list_value, casper){
   var selected_value = random_from_list(list_value);
   var selector = gen_input_with_name_value(name, selected_value);
   if(DEBUG) casper.echo(selector);
   casper.click(selector);
   return true;
};

var helper_check_with_value = function(list_value, capser){
    var selected_value = random_from_list(list_value);
    var selector = gen_input_with_value(selected_value);
    if(DEBUG) capser.echo(selector);
    capser.click(selector);
};

var helper_check_list = function(list_value, prob, max, casper){
    for(idx in list_value){
        if(Math.floor((Math.random() * max)) <= prob){
            var current_value = list_value[idx];
            if(DEBUG) casper.echo(current_value);
            casper.click(gen_input_with_value(current_value));
        }
    }
};

casper.start(link_google, function() {
    if(DEBUG) this.capture('output/0openpage.png');
});

/*
    Page 1
 */
casper.then(function(){
    helper_check_with_value(['Có'], this);

    if(DEBUG) this.capture('output/1x.png');
    this.click('input#ss-submit');
    this.wait(1000);
});


/*
    Page 2
 */
casper.then(function(){
    helper_check_with_value(['Có'], this);

    if(DEBUG) this.capture('output/2x.png');
    this.click('input#ss-submit');
    this.wait(1000);
});


/*
    Page 3
 */
casper.then(function(){
    var ls_page = [
        'Zing Me','Zing Me',
        'Go.vn',
        'Clip.vn',
        'Cyworld.vn',
        'Yume.vn',
        'Tamtay.vn',
        'Phununet.com',
        'Cyvee.vn',
        'Banbe.net',
        'Bang.vn',
        'Truongxua.vn'
    ];
    helper_check_list(ls_page, 4, 10, this);

    var ls_freq = [
        'Mọi ngày trong tuần',
        'Vài tuần một lần','Vài tuần một lần','Vài tuần một lần','Vài tuần một lần',
        'Hiếm khi','Hiếm khi',
        'Trước có sử dụng, giờ thì không','Trước có sử dụng, giờ thì không','Trước có sử dụng, giờ thì không','Trước có sử dụng, giờ thì không'
    ]
    helper_check_with_value(ls_freq, this);

    if(DEBUG) this.capture('output/3x.png');
    this.click('input#ss-submit');
    this.wait(1000);
})


/*
    Page 4
 */
casper.then(function(){

    var ls_purpose = [
        'Kết bạn', 'Kết bạn',
        'Chia sẻ cảm xúc, thông tin',
        'Xem, chia sẻ hình ảnh, video', 'Xem, chia sẻ hình ảnh, video',
        'Đọc tin tức',
        'Chơi game', 'Chơi game',
        'Kinh doanh',
        'Xây dựng thương hiệu cá nhân'
    ];
    helper_check_list(ls_purpose, 4, 10, this);

//    for(idx in ls_purpose){
//        this.click(gen_input_with_value(ls_purpose[idx]));
//    }

    var ls_rating_name = [
        'entry.358100145', //Tinh nang day de
        'entry.1612271693',   //giao dien than thien
        'entry.60097146',    // de su dung
        'entry.125454240',    // thuong xuyen trao doi
        'entry.974601831'    // hiem khi gap loi
    ];
    var ls_rating_point = [
        '1','1','1',
        '2','2','2','2','2','2',
        '3','3','3',
        '4',
        '5'
    ];

    for(idx in ls_rating_name){
        var name_name = ls_rating_name[idx];
        helper_check_with_name_value(name_name, ls_rating_point, this);
    }

    var ls_page = [
        'Zing Me','Zing Me','Zing Me',
        'Go.vn',
        'Clip.vn',
        'Cyworld.vn',
        'Yume.vn',
        'Tamtay.vn',
        'Phununet.com',
        'Cyvee.vn',
        'Banbe.net',
        'Bang.vn',
        'Truongxua.vn'
    ];

//    for(idx in ls_page){
//        this.click(gen_input_with_value(ls_page[idx]));
//    }
    helper_check_list(ls_page, 4, 10, this);


    var ls_reference = [
        'Bạn bè giới thiệu','Bạn bè giới thiệu','Bạn bè giới thiệu',
        'Các kênh thông tin đại chúng: báo chí, quảng cáo, internet,…',
        'Hội thảo, sự kiện',
        'Email marketing'
    ];
    helper_check_list(ls_reference, 4, 10, this);

    var ls_rating_PR = [
        'entry.1920927178', //Các mạng xã hội này rất phổ biến, được nhiều người biết đến
        'entry.1096890005', //Các chiến dịch, hoạt động, sự kiện… do các mạng này tổ chức/tài trợ được quảng bá rầm rộ, phổ biến rộng rãi
        'entry.1484865361', //Nhiều quảng cáo hấp dẫn, thu hút
        'entry.1987536933' //Dịch vụ hỗ trợ khách hàng rất tốt
    ];
    for(idx in ls_rating_PR){
       var name_PR = ls_rating_PR[idx];
       helper_check_with_name_value(name_PR, ls_rating_point, this);
    }

    helper_check_with_value(['Không'], this);

    if(DEBUG) this.capture('output/4x.png');
    this.click('input#ss-submit');
    this.wait(1000);
});


/*
    Page 5
 */
casper.then(function(){
    var gener_name = 'entry.921913705';
    var ls_gender_value = [
        'Nữ',
        'Nam'
    ];
    helper_check_with_name_value(gener_name, ls_gender_value, this);

    var age_name = 'entry.1669310560';
    var ls_age_value = [
        'Dưới 18 tuổi',
        'Từ 18-24 tuổi','Từ 18-24 tuổi','Từ 18-24 tuổi','Từ 18-24 tuổi',
        'Từ 25-30 tuổi','Từ 25-30 tuổi',
        'Trên 30 tuổi'
    ];
    helper_check_with_name_value(age_name, ls_age_value, this);

    var edu_name = 'entry.672595748';
    var ls_edu_value = [
        'Trung học phổ thông',
        'Cao đẳng / Đại học','Cao đẳng / Đại học','Cao đẳng / Đại học','Cao đẳng / Đại học',
        'Trên Đại học'];
    helper_check_with_name_value(edu_name, ls_edu_value, this);

    var job_name = 'entry.1091670586';
    var ls_jbo_value = [
        'Học sinh-sinh viên','Học sinh-sinh viên','Học sinh-sinh viên','Học sinh-sinh viên',
        //'Nhân viên Nhà nước',
        'Nhân viên ngoài Nhà nước',
        'Khác'];
    helper_check_with_name_value(job_name, ls_jbo_value, this);

    if(DEBUG) this.capture('output/5x.png');
    this.click('input#ss-submit');
    this.wait(1000);
});

casper.then(function(){
    if(DEBUG) this.capture('output/6x.png');
    this.echo('Done');
});

casper.wait(1000);
casper.run();
